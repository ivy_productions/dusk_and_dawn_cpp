#pragma once

#include <memory>

#include <rendering/scene/camera.hpp>
#include <rendering/targets/frame.hpp>

#include <bounds/frustum.hpp>

#include "node.hpp"

namespace gid_tech::scene
{
	using namespace bounds;
	using namespace rendering;

	class CameraNode : public Node
	{
		friend class Scene;

	public:
		struct Config
		{
			f32 fov{};
			f32 apsect{};
			f32 near{};
			f32 far{};
		};

	private:
		Config config;

		unique_ptr<Camera> camera;
		shared_ptr<Frame> frame;

		Optional<Matrix<f32, 4, 4>> local_to_projection{OptionalNone};
		Optional<Frustum> frustum{OptionalNone};
		bool needs_upload{false};

	public:
		CameraNode(string name, Transform transform, Config config, shared_ptr<Frame> frame);
		CameraNode(string name, Transform transform, Scene * scene, Config config, shared_ptr<Frame> frame);
		CameraNode(string name, Transform transform, Node * parent, Config config, shared_ptr<Frame> frame);
		CameraNode(string name, Transform transform, Optional<variant<Scene *, Node *>> scene_or_parent, Config config, shared_ptr<Frame> frame);
		virtual ~CameraNode();


		Frame const & GetFrame() const;
		void SetFrame(shared_ptr<Frame> frame);

		Config const & GetConfig();
		void SetConfig(Config config);

		Frustum const & GetFrustum();

		Matrix<f32, 4, 4> GetLocalToProjection();

		Node * Copy(function<void(Node *)> node_alloc_callback, Optional<variant<Scene *, Node *>> scene_or_parent) override;

		Node * Copy(function<void(Node *)> node_alloc_callback, optional<variant<Scene *, Node *>> scene_or_parent) override;

	protected:
		void TransformChanged() override;
		void SceneChanged() override;

	private:
		data::Camera GetCameraData();
		void Upload();
	};
}
