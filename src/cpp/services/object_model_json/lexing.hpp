#include <string>
#include <types/types.hpp>
#include <variant>
#include <vector>

#include "tokens.hpp"

using namespace std;

namespace gid_tech::object_model::json::lexing
{
	using namespace tokens;
	
	using Token = variant<Null, Bool, Int, Float, String, Comma, Colon, LeftBracket, RightBracket, LeftBrace, RightBrace>;

	vector<Token> Lex(string_view json);
}
