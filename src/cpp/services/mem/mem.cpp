#include <types/types.hpp>

using namespace gid_tech::types;

namespace gid_tech::mem
{
	extern "C"
	{
		void * memcpy(void * destination, const void * source, u64 num)
		{
			if (destination == source)
				return destination;

			u8 tmp[num];
			auto src = static_cast<u8 const *>(source);
			auto dest = static_cast<u8 *>(destination);

			for (u64 i = 0; i < num; i++)
				tmp[i] = src[i];
			for (u64 i = 0; i < num; i++)
				dest[i] = tmp[i];

			return destination;
		}

		void * memset(void * ptr, int value, u64 num)
		{
			auto p = static_cast<u8 *>(ptr);

			for (u64 i = 0; i < num; i++)
				p[i] = value;

			return ptr;
		}

		int memcmp(void const * ptr1, void const * ptr2, u64 num)
		{
			auto a = static_cast<u8 const *>(ptr1);
			auto b = static_cast<u8 const *>(ptr2);

			for (u64 i = 0; i < num; i++)
				if (a[i] != b[i])
					return 0;

			return 1;
		}
	}
}
