#include "input.hpp"

namespace gid_tech::windowing
{
	Input::Mouse::Position Input::GetMousePosition() const
	{
		return mouse_current.position;
	}

	Input::Mouse::Position Input::GetMousePositionDelta() const
	{
		return Mouse::Position{
			mouse_current.position.x - mouse_last.position.x,
			mouse_current.position.y - mouse_last.position.y,
		};
	}

	b8 Input::GetMouseButton(Mouse::Button button) const
	{
		return mouse_current.buttons[static_cast<usize>(button)] == Type::Down;
	}

	b8 Input::GetMouseButtonUp(Mouse::Button button) const
	{
		return mouse_current.buttons[static_cast<usize>(button)] == Type::Up && mouse_last.buttons[static_cast<usize>(button)] == Type::Down;
	}

	b8 Input::GetMouseButtonDown(Mouse::Button button) const
	{
		return mouse_current.buttons[static_cast<usize>(button)] == Type::Down && mouse_last.buttons[static_cast<usize>(button)] == Type::Up;
	}

	i64 Input::GetMouseWheel() const
	{
		return mouse_current.wheel;
	}

	i64 Input::GetMouseWheelDelta() const
	{
		return mouse_current.wheel - mouse_last.wheel;
	}

	b8 Input::GetKeyboardKey(Keyboard::Key key) const
	{
		return keyboard_current.keys[static_cast<usize>(key)] == Type::Down;
	}

	b8 Input::GetKeyboardKeyUp(Keyboard::Key key) const
	{
		return keyboard_current.keys[static_cast<usize>(key)] == Type::Up && keyboard_last.keys[static_cast<usize>(key)] == Type::Down;
	}

	b8 Input::GetKeyboardKeyDown(Keyboard::Key key) const
	{
		return keyboard_current.keys[static_cast<usize>(key)] == Type::Down && keyboard_last.keys[static_cast<usize>(key)] == Type::Up;
	}

	string const & Input::GetKeyboardText() const
	{
		return keyboard_current.text;
	}

	b8 Input::ShouldClose() const
	{
		return should_close;
	}

	void Input::SetMousePosition(Mouse::Position position)
	{
		mouse_current.position = position;
	}

	void Input::SetMouseButton(Mouse::Button button, Type type)
	{
		mouse_current.buttons[static_cast<usize>(button)] = type;
	}

	void Input::SetMouseWheel(i64 value)
	{
		mouse_current.wheel = value;
	}

	void Input::SetKeyboardKey(Keyboard::Key key, Type type)
	{
		keyboard_current.keys[static_cast<usize>(key)] = type;
	}

	void Input::SetKeyboardText(string const & text)
	{
		keyboard_current.text = text;
	}

	void Input::SetShouldClose()
	{
		should_close = true;
	}

	void Input::Step()
	{
		mouse_last = mouse_current;
		keyboard_last = keyboard_current;
		keyboard_current.text = "";
	}
}
