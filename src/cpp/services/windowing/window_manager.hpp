#pragma once

#include <collections/array.hpp>
#include <string>
#include <types/types.hpp>

using namespace std;
using namespace gid_tech::types;
using namespace gid_tech::collections;

namespace gid_tech::windowing
{
	class WindowManager
	{
	private:
		struct Implementation;

	private:
		Array<usize, 6> implementation_data{};
		Implementation const & implementation;

	public:
		WindowManager(string identifier);
		~WindowManager();

		WindowManager(WindowManager const &) = delete;
		WindowManager & operator=(WindowManager const &) = delete;


		Implementation const & GetImplementation() const;
		void * GetImplementationHandle() const;
	};

	inline WindowManager::Implementation const & WindowManager::GetImplementation() const
	{
		return implementation;
	}
}
