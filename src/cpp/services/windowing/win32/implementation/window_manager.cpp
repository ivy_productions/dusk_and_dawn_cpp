#ifdef WINDOWS

#include "window_manager.hpp"

#include "window.hpp"

using namespace std;
using namespace gid_tech::types;

namespace gid_tech::windowing::win32
{
	WindowManager::WindowManager(wstring class_name) :
		hinstance(GetModuleHandleW(nullptr)),
		class_name(move(class_name))
	{
		auto wndclass = WNDCLASSW{
			.style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW,
			.lpfnWndProc = WindowProc,
			.cbClsExtra = 0,
			.cbWndExtra = 0,
			.hInstance = hinstance,
			.hIcon = LoadIconW(nullptr, IDI_APPLICATION),
			.hCursor = LoadCursorW(nullptr, IDC_ARROW),
			.hbrBackground = 0,
			.lpszMenuName = nullptr,
			.lpszClassName = this->class_name.data(),
		};
		if (!RegisterClassW(&wndclass))
			exit(EXIT_FAILURE);
	}

	WindowManager::~WindowManager()
	{
		UnregisterClassW(class_name.data(), hinstance);
	}

	HINSTANCE WindowManager::GetHinstance() const
	{
		return hinstance;
	}

	wstring const & WindowManager::GetClassName() const
	{
		return class_name;
	}

	LRESULT WindowManager::WindowProc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
	{
		auto window = reinterpret_cast<Window *>(GetWindowLongPtr(hwnd, GWLP_USERDATA));

		if (window == nullptr)
			return DefWindowProcW(hwnd, msg, wparam, lparam);
		else
			return window->WindowProc(msg, wparam, lparam);
	}
}

#endif
