#ifdef WINDOWS

#include "window.hpp"

#include <utility>

#include <Windows.h>
#undef CreateWindow

#include <types/types.hpp>

#include "window_manager.hpp"

namespace gid_tech::windowing::win32
{
	using std::move;

	Window::Window(WindowManager const & window_manager, string title, Size size) :
		window_manager(window_manager),
		hwnd(CreateHWND(window_manager.GetHinstance(), window_manager.GetClassName(), title, size)),
		title(move(title)),
		size(size)
	{
	}

	Window::~Window()
	{
		DestroyWindow(hwnd);
	}

	HWND Window::GetHWND() const
	{
		return hwnd;
	}

	Input const & Window::GetInput() const
	{
		return input;
	}

	string const & Window::GetTitle() const
	{
		return title;
	}

	Size const & Window::GetSize() const
	{
		return size;
	}

	void Window::Tick()
	{
		input.Step();

		SetWindowLongPtrW(hwnd, GWLP_USERDATA, LONG_PTR(this));
		MSG msg{};
		while (PeekMessageW(&msg, hwnd, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessageW(&msg);
		}
	}

	LRESULT Window::WindowProc(UINT msg, WPARAM wparam, LPARAM lparam)
	{
		if (input.ShouldClose())
			return 0;

		switch (msg)
		{
		// mouse
		case WM_LBUTTONDOWN:
		{
			input.SetMouseButton(Input::Mouse::Button::Left, Input::Type::Down);
			break;
		}
		case WM_LBUTTONUP:
		{
			input.SetMouseButton(Input::Mouse::Button::Left, Input::Type::Up);
			break;
		}
		case WM_RBUTTONDOWN:
		{
			input.SetMouseButton(Input::Mouse::Button::Right, Input::Type::Down);
			break;
		}
		case WM_RBUTTONUP:
		{
			input.SetMouseButton(Input::Mouse::Button::Right, Input::Type::Up);
			break;
		}
		case WM_MBUTTONDOWN:
		{
			input.SetMouseButton(Input::Mouse::Button::Middle, Input::Type::Down);
			break;
		}
		case WM_MBUTTONUP:
		{
			input.SetMouseButton(Input::Mouse::Button::Middle, Input::Type::Up);
			break;
		}
		case WM_MOUSEMOVE:
		{
			input.SetMousePosition(Input::Mouse::Position{LOWORD(lparam), HIWORD(lparam)});
			break;
		}
		case WM_MOUSEWHEEL:
		{
			int64_t delta = GET_WHEEL_DELTA_WPARAM(wparam);
			input.SetMouseWheel(input.GetMouseWheel() + (delta > 0 ? 1 : -1));
			break;
		}

		// keyboard
		case WM_KEYDOWN:
		{
			auto key = ToKey(wparam);
			if (key.IsSome())
				input.SetKeyboardKey(key.Value(), Input::Type::Down);
			break;
		}
		case WM_KEYUP:
		{
			auto key = ToKey(wparam);
			if (key.IsSome())
				input.SetKeyboardKey(key.Value(), Input::Type::Up);
			break;
		}
		case WM_CHAR:
		{
			char c = (char) wparam;
			input.SetKeyboardText(input.GetKeyboardText() + c);
			break;
		}

		// window
		case WM_CLOSE:
		{
			input.SetShouldClose();
			break;
		}

		// ----
		default:
			return DefWindowProcW(hwnd, msg, wparam, lparam);
		}
		return 0;
	}

	HWND Window::CreateHWND(HINSTANCE hinstance, wstring const & class_name, string const & title, Size size)
	{
		auto w_title = wstring(title.begin(), title.end());

		RECT rect{0, 0, static_cast<LONG>(size.width), static_cast<LONG>(size.height)};
		AdjustWindowRectEx(&rect, WS_OVERLAPPEDWINDOW | WS_VISIBLE, FALSE, 0L);

		auto hwnd = CreateWindowExW(
			0L,
			class_name.c_str(),
			w_title.c_str(),
			WS_OVERLAPPEDWINDOW | WS_VISIBLE,
			CW_USEDEFAULT, CW_USEDEFAULT, rect.right - rect.left, rect.bottom - rect.top,
			nullptr, nullptr,
			hinstance,
			0);
		ShowWindow(hwnd, TRUE);
		return hwnd;
	}

	Optional<Input::Keyboard::Key> Window::ToKey(WPARAM wparam)
	{
		switch (wparam)
		{
		case 0x31:
			return Input::Keyboard::Key::Key1;
		case 0x32:
			return Input::Keyboard::Key::Key2;
		case 0x33:
			return Input::Keyboard::Key::Key3;
		case 0x34:
			return Input::Keyboard::Key::Key4;
		case 0x35:
			return Input::Keyboard::Key::Key5;
		case 0x36:
			return Input::Keyboard::Key::Key6;
		case 0x37:
			return Input::Keyboard::Key::Key7;
		case 0x38:
			return Input::Keyboard::Key::Key8;
		case 0x39:
			return Input::Keyboard::Key::Key9;
		case 0x30:
			return Input::Keyboard::Key::Key0;

		case 0x41:
			return Input::Keyboard::Key::A;
		case 0x42:
			return Input::Keyboard::Key::B;
		case 0x43:
			return Input::Keyboard::Key::C;
		case 0x44:
			return Input::Keyboard::Key::D;
		case 0x45:
			return Input::Keyboard::Key::E;
		case 0x46:
			return Input::Keyboard::Key::F;
		case 0x47:
			return Input::Keyboard::Key::G;
		case 0x48:
			return Input::Keyboard::Key::H;
		case 0x49:
			return Input::Keyboard::Key::I;
		case 0x4a:
			return Input::Keyboard::Key::J;
		case 0x4b:
			return Input::Keyboard::Key::K;
		case 0x4c:
			return Input::Keyboard::Key::L;
		case 0x4d:
			return Input::Keyboard::Key::M;
		case 0x4e:
			return Input::Keyboard::Key::N;
		case 0x4f:
			return Input::Keyboard::Key::O;
		case 0x50:
			return Input::Keyboard::Key::P;
		case 0x51:
			return Input::Keyboard::Key::Q;
		case 0x52:
			return Input::Keyboard::Key::R;
		case 0x53:
			return Input::Keyboard::Key::S;
		case 0x54:
			return Input::Keyboard::Key::T;
		case 0x55:
			return Input::Keyboard::Key::U;
		case 0x56:
			return Input::Keyboard::Key::V;
		case 0x57:
			return Input::Keyboard::Key::W;
		case 0x58:
			return Input::Keyboard::Key::X;
		case 0x59:
			return Input::Keyboard::Key::Y;
		case 0x5a:
			return Input::Keyboard::Key::Z;

		case VK_SPACE:
			return Input::Keyboard::Key::Space;
		case VK_OEM_3:
			return Input::Keyboard::Key::GraveAccent;
		case VK_OEM_MINUS:
			return Input::Keyboard::Key::Minus;
		case VK_OEM_PLUS:
			return Input::Keyboard::Key::Equal;
		case VK_OEM_4:
			return Input::Keyboard::Key::LeftBracket;
		case VK_OEM_6:
			return Input::Keyboard::Key::RightBracket;
		case VK_OEM_1:
			return Input::Keyboard::Key::Semicolon;
		case VK_OEM_7:
			return Input::Keyboard::Key::Apostrophe;
		case VK_OEM_5:
			return Input::Keyboard::Key::BackSlash;
		case VK_OEM_COMMA:
			return Input::Keyboard::Key::Comma;
		case VK_OEM_PERIOD:
			return Input::Keyboard::Key::Period;
		case VK_OEM_2:
			return Input::Keyboard::Key::Slash;

		case VK_SHIFT:
			return Input::Keyboard::Key::Shift;
		case VK_CONTROL:
			return Input::Keyboard::Key::Control;
		case VK_MENU:
			return Input::Keyboard::Key::Alt;
		case VK_LWIN:
		case VK_RWIN:
			return Input::Keyboard::Key::Super;

		case VK_TAB:
			return Input::Keyboard::Key::Tab;
		case VK_CAPITAL:
			return Input::Keyboard::Key::CapsLock;
		case VK_BACK:
			return Input::Keyboard::Key::BackSpace;
		case VK_RETURN:
			return Input::Keyboard::Key::Enter;

		case VK_ESCAPE:
			return Input::Keyboard::Key::Escape;

		case VK_F1:
			return Input::Keyboard::Key::F1;
		case VK_F2:
			return Input::Keyboard::Key::F2;
		case VK_F3:
			return Input::Keyboard::Key::F3;
		case VK_F4:
			return Input::Keyboard::Key::F4;
		case VK_F5:
			return Input::Keyboard::Key::F5;
		case VK_F6:
			return Input::Keyboard::Key::F6;
		case VK_F7:
			return Input::Keyboard::Key::F7;
		case VK_F8:
			return Input::Keyboard::Key::F8;
		case VK_F9:
			return Input::Keyboard::Key::F9;
		case VK_F10:
			return Input::Keyboard::Key::F10;
		case VK_F11:
			return Input::Keyboard::Key::F11;
		case VK_F12:
			return Input::Keyboard::Key::F12;

		case VK_INSERT:
			return Input::Keyboard::Key::Insert;
		case VK_DELETE:
			return Input::Keyboard::Key::Delete;
		case VK_HOME:
			return Input::Keyboard::Key::Home;
		case VK_END:
			return Input::Keyboard::Key::End;
		case VK_PRIOR:
			return Input::Keyboard::Key::PageUp;
		case VK_NEXT:
			return Input::Keyboard::Key::PageDown;

		case VK_LEFT:
			return Input::Keyboard::Key::Left;
		case VK_RIGHT:
			return Input::Keyboard::Key::Right;
		case VK_UP:
			return Input::Keyboard::Key::Up;
		case VK_DOWN:
			return Input::Keyboard::Key::Down;

		case VK_NUMLOCK:
			return Input::Keyboard::Key::NumLock;
		case VK_NUMPAD0:
			return Input::Keyboard::Key::Num0;
		case VK_NUMPAD1:
			return Input::Keyboard::Key::Num1;
		case VK_NUMPAD2:
			return Input::Keyboard::Key::Num2;
		case VK_NUMPAD3:
			return Input::Keyboard::Key::Num3;
		case VK_NUMPAD4:
			return Input::Keyboard::Key::Num4;
		case VK_NUMPAD5:
			return Input::Keyboard::Key::Num5;
		case VK_NUMPAD6:
			return Input::Keyboard::Key::Num6;
		case VK_NUMPAD7:
			return Input::Keyboard::Key::Num7;
		case VK_NUMPAD8:
			return Input::Keyboard::Key::Num8;
		case VK_NUMPAD9:
			return Input::Keyboard::Key::Num9;
		case VK_DIVIDE:
			return Input::Keyboard::Key::NumDivide;
		case VK_MULTIPLY:
			return Input::Keyboard::Key::NumMultiply;
		case VK_SUBTRACT:
			return Input::Keyboard::Key::NumSubtract;
		case VK_ADD:
			return Input::Keyboard::Key::NumAdd;
		case VK_DECIMAL:
			return Input::Keyboard::Key::NumDecimal;
		default:
			return OptionalNone;
		}
	}
}

#endif
