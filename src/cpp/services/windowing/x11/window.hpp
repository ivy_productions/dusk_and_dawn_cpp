#pragma once

#ifdef LINUX

#include "../window.hpp"
#include "implementation/window.hpp"

namespace gid_tech::windowing
{
	struct Window::Implementation
	{
		x11::Window window;
	};
}

#endif
