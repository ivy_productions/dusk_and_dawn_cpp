#include "window_manager.hpp"

#ifdef LINUX

namespace gid_tech::windowing::x11
{
	WindowManager::WindowManager() :
		display(XOpenDisplay(nullptr))
	{
	}

	WindowManager::~WindowManager()
	{
		XCloseDisplay(display);
	}

	::Display * WindowManager::GetDisplay() const
	{
		return display;
	}
}

#endif
