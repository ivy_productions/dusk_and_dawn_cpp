#pragma once

#include <string>

#include <collections/array.hpp>
#include <types/types.hpp>

#include "input.hpp"
#include "size.hpp"

using namespace std;
using namespace gid_tech::types;
using namespace gid_tech::collections;

namespace gid_tech::windowing
{
	class WindowManager;

	class Window
	{
	public:
		struct Implementation;

	public:
		WindowManager const & window_manager;

		Array<usize, 128 * 3> const implementation_data{};
		Implementation & implementation;

	public:
		Window(WindowManager const & window_manager, string title, Size size);
		~Window();

		Window(Window const &) = delete;
		Window & operator=(Window const &) = delete;


		Input const & GetInput() const;
		string const & GetTitle() const;
		Size const & GetSize() const;

		void Tick();

		Implementation const & GetImplementation() const;
		void * GetImplementationHandle() const;
	};

	inline Window::Implementation const & Window::GetImplementation() const
	{
		return implementation;
	}
}
