#pragma once

#include <collections/array.hpp>
#include <types/traits.hpp>
#include <types/types.hpp>

using namespace gid_tech::types;
using namespace gid_tech::collections;

namespace gid_tech::algebra
{
#pragma region // data

	template<typename T, usize X, usize Y, typename Enable = void>
	struct MatrixData
	{
		union
		{
			Array<Array<T, X>, Y> columns;
			Array<T, X * Y> elements;
			T columns_raw[Y][X];
			T elements_raw[X * Y]{};
		};
	};

	// zero size matrix
	template<typename T, usize X, usize Y>
	struct MatrixData<T, X, Y, typename EnableIf<X == 0 || Y == 0>::Type>
	{
	};

	// vector specializations
	template<typename T, usize X, usize Y>
	struct MatrixData<T, X, Y, typename EnableIf<X == 1 && Y == 1>::Type>
	{
		union
		{
			struct
			{
				T x;
			};
			Array<Array<T, X>, Y> columns;
			Array<T, X * Y> elements;
			T columns_raw[Y][X];
			T elements_raw[X * Y]{};
		};
	};

	template<typename T, usize X, usize Y>
	struct MatrixData<T, X, Y, typename EnableIf<(X == 1 || Y == 1) && (X == 2 || Y == 2)>::Type>
	{
		union
		{
			struct
			{
				T x;
				T y;
			};
			Array<Array<T, X>, Y> columns;
			Array<T, X * Y> elements;
			T columns_raw[Y][X];
			T elements_raw[X * Y]{};
		};
	};

	template<typename T, usize X, usize Y>
	struct MatrixData<T, X, Y, typename EnableIf<(X == 1 || Y == 1) && (X == 3 || Y == 3)>::Type>
	{
		union
		{
			struct
			{
				T x;
				T y;
				T z;
			};
			Array<Array<T, X>, Y> columns;
			Array<T, X * Y> elements;
			T columns_raw[Y][X];
			T elements_raw[X * Y]{};
		};
	};

	template<typename T, usize X, usize Y>
	struct MatrixData<T, X, Y, typename EnableIf<(X == 1 || Y == 1) && (X == 4 || Y == 4)>::Type>
	{
		union
		{
			struct
			{
				T x;
				T y;
				T z;
				T w;
			};
			Array<Array<T, X>, Y> columns;
			Array<T, X * Y> elements;
			T columns_raw[Y][X];
			T elements_raw[X * Y]{};
		};
	};

#pragma endregion

	template<typename T, usize X, usize Y>
	struct Matrix final : MatrixData<T, X, Y>
	{
		static_assert(IsIntegral<T>::value, "only integral types allowed");

		using ElementType = T;
		static constexpr usize dimension_x = X;
		static constexpr usize dimension_y = Y;

		Matrix() = default;
		Matrix(Matrix<T, X, Y> const &) = default;
		template<usize X2, usize Y2>
		Matrix(Matrix<T, X2, Y2> const & other);

		Matrix<T, X, Y> & operator=(Matrix<T, X, Y> const &) = default;
		template<usize X2, usize Y2>
		Matrix<T, X, Y> & operator=(Matrix<T, X2, Y2> const & other);

		Array<T, X> & operator[](usize index);
		Array<T, X> const & operator[](usize index) const;

		template<usize M2Y>
		Matrix<T, X, M2Y> operator*(Matrix<T, Y, M2Y> const & other) const;

		Matrix<T, X, Y> operator+(Matrix<T, X, Y> const & other) const;
		Matrix<T, X, Y> operator-(Matrix<T, X, Y> const & other) const;
		Matrix<T, X, Y> operator*(T other) const;
		Matrix<T, X, Y> operator/(T other) const;

		Matrix<T, X, Y> & operator+=(Matrix<T, X, Y> const & other);
		Matrix<T, X, Y> & operator-=(Matrix<T, X, Y> const & other);
		Matrix<T, X, Y> & operator*=(T other);
		Matrix<T, X, Y> & operator/=(T other);

		bool operator==(Matrix<T, X, Y> const & other) const;
		bool operator!=(Matrix<T, X, Y> const & other) const;

		Matrix<T, X - 1, Y - 1> Minor(usize index_x, usize index_y) const;
		T Trace() const;
		Matrix<T, Y, X> Transposed() const;
		Matrix<T, X, Y> ComponentMultiplied(Matrix<T, X, Y> const & other) const;

		Matrix<T, X, 1> GetColumn(usize column) const;
		Matrix<T, Y, 1> GetRow(usize row) const;

		Matrix<T, X, Y> Min(Matrix<T, X, Y> const & other) const;
		Matrix<T, X, Y> Max(Matrix<T, X, Y> const & other) const;

		static constexpr Matrix<T, X, Y> Diagonal(T value);
		static constexpr Matrix<T, X, Y> Identity();

#pragma region // square

		template<usize X2 = X, usize Y2 = Y, typename = typename EnableIf<X2 == Y2>::Type>
		Matrix<T, X, Y> & Transpose();

		template<usize X2 = X, usize Y2 = Y, typename = typename EnableIf<(X2 == Y2) && (X2 == 1)>::Type>
		T Determinante() const;
		template<usize X2 = X, usize Y2 = Y, typename = typename EnableIf<(X2 == Y2) && (X2 >= 2)>::Type>
		T Determinante(int = 0) const;

		template<usize X2 = X, usize Y2 = Y, typename = typename EnableIf<(X2 == Y2) && (X2 != 4)>::Type>
		Matrix<T, X, Y> & Invert();
		template<usize X2 = X, usize Y2 = Y, typename = typename EnableIf<(X2 == Y2) && (X2 == 4)>::Type>
		Matrix<T, X, Y> & Invert(int = 0);
		template<usize X2 = X, usize Y2 = Y, typename = typename EnableIf<X2 == Y2>::Type>
		Matrix<T, X, Y> Inverse() const;

#pragma endregion

#pragma region // vector

		template<usize X2 = X, usize Y2 = Y, typename = typename EnableIf<X2 == 1 || Y2 == 1>::Type>
		T LengthSquared() const;
		template<usize X2 = X, usize Y2 = Y, typename = typename EnableIf<X2 == 1 || Y2 == 1>::Type>
		T Length() const;
		template<usize X2 = X, usize Y2 = Y, typename = typename EnableIf<X2 == 1 || Y2 == 1>::Type>
		Matrix<T, X, Y> & Normalize();
		template<usize X2 = X, usize Y2 = Y, typename = typename EnableIf<X2 == 1 || Y2 == 1>::Type>
		Matrix<T, X, Y> Normalized() const;
		template<usize X2 = X, usize Y2 = Y, typename = typename EnableIf<X2 == 1 || Y2 == 1>::Type>
		T Dot(Matrix<T, X, Y> const & other) const;

#pragma endregion

#pragma region // vector1

		template<usize X2 = X, usize Y2 = Y, typename = typename EnableIf<(X2 == 1 || Y2 == 1) && (X2 == 1 || Y2 == 1)>::Type>
		Matrix(T x);

#pragma endregion

#pragma region // vector2

		template<usize X2 = X, usize Y2 = Y, typename = typename EnableIf<(X2 == 1 || Y2 == 1) && (X2 == 2 || Y2 == 2)>::Type>
		Matrix(T x, T y);

#pragma endregion

#pragma region // vector3

		template<usize X2 = X, usize Y2 = Y, typename = typename EnableIf<(X2 == 1 || Y2 == 1) && (X2 == 3 || Y2 == 3)>::Type>
		Matrix(T x, T y, T z);

		template<usize X2 = X, usize Y2 = Y, typename = typename EnableIf<(X2 == 1 || Y2 == 1) && (X2 == 3 || Y2 == 3)>::Type>
		Matrix<T, X, Y> Cross(Matrix<T, X, Y> const & other) const;

#pragma endregion

#pragma region // vector4

		template<usize X2 = X, usize Y2 = Y, typename = typename EnableIf<(X2 == 1 || Y2 == 1) && (X2 == 4 || Y2 == 4)>::Type>
		Matrix(T x, T y, T z, T w);

#pragma endregion
	};

}

#include "matrix.impl.hpp"
