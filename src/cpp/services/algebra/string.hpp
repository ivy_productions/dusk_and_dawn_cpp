#pragma once

#include "matrix.hpp"
#include "quaternion.hpp"

#include <string>

using namespace gid_tech::types;

namespace gid_tech::algebra
{
	using std::string;

	template<typename T, usize X, usize Y>
	string ToString(Matrix<T, X, Y> const & matrix);
	template<typename T, usize X>
	string ToString(Vector<T, X> const & vector);
	template<typename T>
	string ToString(Quaternion<T> const & qutaernion);
}

#include "string.impl.hpp"
