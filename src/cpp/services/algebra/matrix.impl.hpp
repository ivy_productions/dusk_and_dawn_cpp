#pragma once

#include "matrix.hpp"

#include <algorithm>
#include <math/math.hpp>

using namespace std;
using namespace gid_tech::math;

namespace gid_tech::algebra
{
	template<typename T, usize X, usize Y>
	template<usize X2, usize Y2>
	Matrix<T, X, Y>::Matrix(Matrix<T, X2, Y2> const & other) :
		Matrix()
	{
		for (usize i = 0; i < min(Y, Y2); i++)
		{
			for (usize j = 0; j < min(X, X2); j++)
			{
				this->columns[i][j] = other.columns[i][j];
			}
		}
	}

	template<typename T, usize X, usize Y>
	template<usize X2, usize Y2>
	Matrix<T, X, Y> & Matrix<T, X, Y>::operator=(Matrix<T, X2, Y2> const & other)
	{
		*this = Matrix<T, X, Y>(other);
		return *this;
	}

	template<typename T, usize X, usize Y>
	Array<T, X> & Matrix<T, X, Y>::operator[](usize index)
	{
		return this->columns[index];
	}

	template<typename T, usize X, usize Y>
	Array<T, X> const & Matrix<T, X, Y>::operator[](usize index) const
	{
		return this->columns[index];
	}

	template<typename T, usize X, usize Y>
	template<usize M2Y>
	Matrix<T, X, M2Y> Matrix<T, X, Y>::operator*(Matrix<T, Y, M2Y> const & other) const
	{
		Matrix<T, X, M2Y> ret;
		for (usize row = 0; row < X; row++)
		{
			for (usize col = 0; col < M2Y; col++)
			{
				for (usize el = 0; el < Y; el++)
				{
					ret[col][row] += this->columns[el][row] * other[col][el];
				}
			}
		}
		return ret;
	}

	template<typename T, usize X, usize Y>
	Matrix<T, X, Y> Matrix<T, X, Y>::operator+(Matrix<T, X, Y> const & other) const
	{
		return Matrix<T, X, Y>(*this) += other;
	}

	template<typename T, usize X, usize Y>
	Matrix<T, X, Y> Matrix<T, X, Y>::operator-(Matrix<T, X, Y> const & other) const
	{
		return Matrix<T, X, Y>(*this) -= other;
	}

	template<typename T, usize X, usize Y>
	Matrix<T, X, Y> Matrix<T, X, Y>::operator*(T other) const
	{
		return Matrix<T, X, Y>(*this) *= other;
	}

	template<typename T, usize X, usize Y>
	Matrix<T, X, Y> Matrix<T, X, Y>::operator/(T other) const
	{
		return Matrix<T, X, Y>(*this) /= other;
	}

	template<typename T, usize X, usize Y>
	Matrix<T, X, Y> & Matrix<T, X, Y>::operator+=(const Matrix<T, X, Y> & other)
	{
		for (usize i = 0; i < X * Y; i++)
			this->elements[i] += other.elements[i];
		return *this;
	}

	template<typename T, usize X, usize Y>
	Matrix<T, X, Y> & Matrix<T, X, Y>::operator-=(const Matrix<T, X, Y> & other)
	{
		for (usize i = 0; i < X * Y; i++)
			this->elements[i] -= other.elements[i];
		return *this;
	}

	template<typename T, usize X, usize Y>
	Matrix<T, X, Y> & Matrix<T, X, Y>::operator*=(T other)
	{
		for (usize i = 0; i < X * Y; ++i)
			this->elements[i] *= other;
		return *this;
	}

	template<typename T, usize X, usize Y>
	Matrix<T, X, Y> & Matrix<T, X, Y>::operator/=(T other)
	{
		for (usize i = 0; i < X * Y; ++i)
			this->elements[i] /= other;
		return *this;
	}

	template<typename T, usize X, usize Y>
	bool Matrix<T, X, Y>::operator==(Matrix<T, X, Y> const & other) const
	{
		return this->elements == other.elements;
	}

	template<typename T, usize X, usize Y>
	bool Matrix<T, X, Y>::operator!=(Matrix<T, X, Y> const & other) const
	{
		return this->elements != other.elements;
	}

	template<typename T, usize X, usize Y>
	Matrix<T, X - 1, Y - 1> Matrix<T, X, Y>::Minor(usize index_x, usize index_y) const
	{
		Matrix<T, X - 1, Y - 1> ret;
		usize skip_y = 0;
		for (usize i = 0; i < Y - 1; i++)
		{
			skip_y += (i == index_y ? 1 : 0);
			usize skip_x = 0;
			for (usize j = 0; j < X - 1; j++)
			{
				skip_x += (j == index_x ? 1 : 0);
				ret[i][j] = (*this)[i + skip_y][j + skip_x];
			}
		}
		return ret;
	}

	template<typename T, usize X, usize Y>
	T Matrix<T, X, Y>::Trace() const
	{
		T ret = 0;
		usize m = min(X, Y);
		for (usize i = 0; i < m; i++)
			ret += (*this)[i][i];
		return ret;
	}

	template<typename T, usize X, usize Y>
	Matrix<T, Y, X> Matrix<T, X, Y>::Transposed() const
	{
		Matrix<T, Y, X> ret;
		for (usize i = 0; i < Y; ++i)
		{
			for (usize j = 0; j < X; ++j)
			{
				ret[j][i] = (*this)[i][j];
			}
		}
		return ret;
	}

	template<typename T, usize X, usize Y>
	Matrix<T, X, Y> Matrix<T, X, Y>::ComponentMultiplied(Matrix<T, X, Y> const & other) const
	{
		Matrix<T, X, Y> ret;
		for (usize i = 0; i < X * Y; i++)
			ret.elements[i] = this->elements[i] * other.elements[i];
		return ret;
	}

	template<typename T, usize X, usize Y>
	Matrix<T, X, 1> Matrix<T, X, Y>::GetColumn(usize column) const
	{
		Matrix<T, X, 1> ret;
		for (usize i = 0; i < Y; i++)
			ret[0][i] = this->columns[column][i];
		return ret;
	}

	template<typename T, usize X, usize Y>
	Matrix<T, Y, 1> Matrix<T, X, Y>::GetRow(usize row) const
	{
		Matrix<T, Y, 1> ret;
		for (usize i = 0; i < Y; i++)
			ret[0][i] = this->columns[i][row];
		return ret;
	}

	template<typename T, usize X, usize Y>
	Matrix<T, X, Y> Matrix<T, X, Y>::Max(Matrix<T, X, Y> const & other) const
	{
		Matrix<T, X, Y> ret;
		for (usize i = 0; i < X * Y; i++)
			ret.elements[i] = max(this->elements[i], other.elements[i]);
		return ret;
	}

	template<typename T, usize X, usize Y>
	Matrix<T, X, Y> Matrix<T, X, Y>::Min(Matrix<T, X, Y> const & other) const
	{
		Matrix<T, X, Y> ret;
		for (usize i = 0; i < X * Y; i++)
			ret.elements[i] = min(this->elements[i], other.elements[i]);
		return ret;
	}

	template<typename T, usize X, usize Y>
	template<usize, usize, typename>
	Matrix<T, X, Y> & Matrix<T, X, Y>::Transpose()
	{
		for (usize i = 0; i < Y; i++)
		{
			for (usize j = i + 1; j < X; j++)
			{
				T temp = this->columns[i][j];
				this->columns[i][j] = this->columns[j][i];
				this->columns[j][i] = temp;
			}
		}
		return *this;
	}

	template<typename T, usize X, usize Y>
	constexpr Matrix<T, X, Y> Matrix<T, X, Y>::Diagonal(T value)
	{
		Matrix<T, X, Y> ret;
		for (usize i = 0; i < X * Y; i += X + 1)
			ret.elements[i] = value;
		return ret;
	}

	template<typename T, usize X, usize Y>
	constexpr Matrix<T, X, Y> Matrix<T, X, Y>::Identity()
	{
		return Matrix<T, X, Y>::Diagonal(T(1));
	}

	template<typename T, usize X, usize Y>
	template<usize, usize, typename>
	T Matrix<T, X, Y>::Determinante() const
	{
		return this->elements[0];
	}

	template<typename T, usize X, usize Y>
	template<usize, usize, typename>
	T Matrix<T, X, Y>::Determinante(int) const
	{
		T ret = T();
		for (usize i = 0; i < X; ++i)
		{
			T sign = i % 2 == 0 ? 1.0 : -1.0;
			ret += this->columns[i][0] * this->Minor(0, i).Determinante() * sign;
		}
		return ret;
	}

	template<typename T, usize X, usize Y>
	template<usize, usize, typename>
	Matrix<T, X, Y> & Matrix<T, X, Y>::Invert()
	{
		Matrix<T, X, Y> inv;

		for (usize i = 0; i < X; ++i)
		{
			for (usize j = 0; j < X; ++j)
			{
				T sign = i % 2 == j % 2 ? 1.0 : -1.0;
				inv[i][j] = this->Minor(j, i).Determinante() * sign;
			}
		}
		inv.Transpose();

		T inv_det{};
		for (usize i = 0; i < X; ++i)
			inv_det += this->columns[0][i] * inv.columns[i][0];
		auto det = T(1) / inv_det;
		// assert(det != 0);

		inv *= det;
		*this = inv;
		return *this;
	}

	template<typename T, usize X, usize Y>
	template<usize X2, usize Y2, typename>
	Matrix<T, X, Y> & Matrix<T, X, Y>::Invert(int)
	{
		auto & m = *this;
		Matrix<T, X, Y> inv;

		inv[0][0] = +m[1][1] * m[2][2] * m[3][3] - m[1][1] * m[2][3] * m[3][2] - m[2][1] * m[1][2] * m[3][3] + m[2][1] * m[1][3] * m[3][2] + m[3][1] * m[1][2] * m[2][3] - m[3][1] * m[1][3] * m[2][2];
		inv[1][0] = -m[1][0] * m[2][2] * m[3][3] + m[1][0] * m[2][3] * m[3][2] + m[2][0] * m[1][2] * m[3][3] - m[2][0] * m[1][3] * m[3][2] - m[3][0] * m[1][2] * m[2][3] + m[3][0] * m[1][3] * m[2][2];
		inv[2][0] = +m[1][0] * m[2][1] * m[3][3] - m[1][0] * m[2][3] * m[3][1] - m[2][0] * m[1][1] * m[3][3] + m[2][0] * m[1][3] * m[3][1] + m[3][0] * m[1][1] * m[2][3] - m[3][0] * m[1][3] * m[2][1];
		inv[3][0] = -m[1][0] * m[2][1] * m[3][2] + m[1][0] * m[2][2] * m[3][1] + m[2][0] * m[1][1] * m[3][2] - m[2][0] * m[1][2] * m[3][1] - m[3][0] * m[1][1] * m[2][2] + m[3][0] * m[1][2] * m[2][1];
		inv[0][1] = -m[0][1] * m[2][2] * m[3][3] + m[0][1] * m[2][3] * m[3][2] + m[2][1] * m[0][2] * m[3][3] - m[2][1] * m[0][3] * m[3][2] - m[3][1] * m[0][2] * m[2][3] + m[3][1] * m[0][3] * m[2][2];
		inv[1][1] = +m[0][0] * m[2][2] * m[3][3] - m[0][0] * m[2][3] * m[3][2] - m[2][0] * m[0][2] * m[3][3] + m[2][0] * m[0][3] * m[3][2] + m[3][0] * m[0][2] * m[2][3] - m[3][0] * m[0][3] * m[2][2];
		inv[2][1] = -m[0][0] * m[2][1] * m[3][3] + m[0][0] * m[2][3] * m[3][1] + m[2][0] * m[0][1] * m[3][3] - m[2][0] * m[0][3] * m[3][1] - m[3][0] * m[0][1] * m[2][3] + m[3][0] * m[0][3] * m[2][1];
		inv[3][1] = +m[0][0] * m[2][1] * m[3][2] - m[0][0] * m[2][2] * m[3][1] - m[2][0] * m[0][1] * m[3][2] + m[2][0] * m[0][2] * m[3][1] + m[3][0] * m[0][1] * m[2][2] - m[3][0] * m[0][2] * m[2][1];
		inv[0][2] = +m[0][1] * m[1][2] * m[3][3] - m[0][1] * m[1][3] * m[3][2] - m[1][1] * m[0][2] * m[3][3] + m[1][1] * m[0][3] * m[3][2] + m[3][1] * m[0][2] * m[1][3] - m[3][1] * m[0][3] * m[1][2];
		inv[1][2] = -m[0][0] * m[1][2] * m[3][3] + m[0][0] * m[1][3] * m[3][2] + m[1][0] * m[0][2] * m[3][3] - m[1][0] * m[0][3] * m[3][2] - m[3][0] * m[0][2] * m[1][3] + m[3][0] * m[0][3] * m[1][2];
		inv[2][2] = +m[0][0] * m[1][1] * m[3][3] - m[0][0] * m[1][3] * m[3][1] - m[1][0] * m[0][1] * m[3][3] + m[1][0] * m[0][3] * m[3][1] + m[3][0] * m[0][1] * m[1][3] - m[3][0] * m[0][3] * m[1][1];
		inv[3][2] = -m[0][0] * m[1][1] * m[3][2] + m[0][0] * m[1][2] * m[3][1] + m[1][0] * m[0][1] * m[3][2] - m[1][0] * m[0][2] * m[3][1] - m[3][0] * m[0][1] * m[1][2] + m[3][0] * m[0][2] * m[1][1];
		inv[0][3] = -m[0][1] * m[1][2] * m[2][3] + m[0][1] * m[1][3] * m[2][2] + m[1][1] * m[0][2] * m[2][3] - m[1][1] * m[0][3] * m[2][2] - m[2][1] * m[0][2] * m[1][3] + m[2][1] * m[0][3] * m[1][2];
		inv[1][3] = +m[0][0] * m[1][2] * m[2][3] - m[0][0] * m[1][3] * m[2][2] - m[1][0] * m[0][2] * m[2][3] + m[1][0] * m[0][3] * m[2][2] + m[2][0] * m[0][2] * m[1][3] - m[2][0] * m[0][3] * m[1][2];
		inv[2][3] = -m[0][0] * m[1][1] * m[2][3] + m[0][0] * m[1][3] * m[2][1] + m[1][0] * m[0][1] * m[2][3] - m[1][0] * m[0][3] * m[2][1] - m[2][0] * m[0][1] * m[1][3] + m[2][0] * m[0][3] * m[1][1];
		inv[3][3] = +m[0][0] * m[1][1] * m[2][2] - m[0][0] * m[1][2] * m[2][1] - m[1][0] * m[0][1] * m[2][2] + m[1][0] * m[0][2] * m[2][1] + m[2][0] * m[0][1] * m[1][2] - m[2][0] * m[0][2] * m[1][1];

		auto inv_det = m[0][0] * inv[0][0] + m[0][1] * inv[1][0] + m[0][2] * inv[2][0] + m[0][3] * inv[3][0];
		// assert(inv_det != 0);
		auto det = T(1) / inv_det;

		inv *= det;
		*this = inv;
		return *this;
	}

	template<typename T, usize X, usize Y>
	template<usize X2, usize Y2, typename>
	Matrix<T, X, Y> Matrix<T, X, Y>::Inverse() const
	{
		return Matrix<T, X, X>(*this).Invert();
	}

	template<typename T, usize X, usize Y>
	template<usize X2, usize Y2, typename>
	T Matrix<T, X, Y>::LengthSquared() const
	{
		T pows = T();
		for (usize i = 0; i < X * Y; i++)
			pows += this->elements[i] * this->elements[i];
		return pows;
	}

	template<typename T, usize X, usize Y>
	template<usize X2, usize Y2, typename>
	T Matrix<T, X, Y>::Length() const
	{
		return Sqrt(this->LengthSquared());
	}

	template<typename T, usize X, usize Y>
	template<usize X2, usize Y2, typename>
	Matrix<T, X, Y> & Matrix<T, X, Y>::Normalize()
	{
		return *this /= Length();
	}

	template<typename T, usize X, usize Y>
	template<usize X2, usize Y2, typename>
	Matrix<T, X, Y> Matrix<T, X, Y>::Normalized() const
	{
		return Matrix<T, X, 1>(*this).Normalize();
	}

	template<typename T, usize X, usize Y>
	template<usize X2, usize Y2, typename>
	T Matrix<T, X, Y>::Dot(Matrix<T, X, Y> const & other) const
	{
		return (*reinterpret_cast<const Matrix<T, Y, X> *>(&other) * *this).x;
	}

	template<typename T, usize X, usize Y>
	template<usize X2, usize Y2, typename>
	Matrix<T, X, Y>::Matrix(T x)
	{
		this->x = x;
	}

	template<typename T, usize X, usize Y>
	template<usize X2, usize Y2, typename>
	Matrix<T, X, Y>::Matrix(T x, T y)
	{
		this->x = x;
		this->y = y;
	}

	template<typename T, usize X, usize Y>
	template<usize X2, usize Y2, typename>
	Matrix<T, X, Y>::Matrix(T x, T y, T z)
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}

	template<typename T, usize X, usize Y>
	template<usize X2, usize Y2, typename>
	Matrix<T, X, Y> Matrix<T, X, Y>::Cross(Matrix<T, X, Y> const & other) const
	{
		Matrix<T, X, Y> ret;
		ret.x = this->y * other.z - this->z * other.y;
		ret.y = this->z * other.x - this->x * other.z;
		ret.z = this->x * other.y - this->y * other.x;
		return ret;
	}

	template<typename T, usize X, usize Y>
	template<usize X2, usize Y2, typename>
	Matrix<T, X, Y>::Matrix(T x, T y, T z, T w)
	{
		this->x = x;
		this->y = y;
		this->z = z;
		this->w = w;
	}
}
