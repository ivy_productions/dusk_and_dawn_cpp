#include "escaping.hpp"

namespace gid_tech::string_utils
{
	string Escape(string const & s)
	{
		string ret;
		ret.reserve(s.size());
		for (auto c : s)
		{
			switch (c)
			{
			case '\n': ret += "\\n"; break;
			case '\r': ret += "\\r"; break;
			case '\t': ret += "\\t"; break;
			case '\"': ret += "\\\""; break;
			case '\\': ret += "\\\\"; break;
			default: ret += c;
			}
		}
		return ret;
	}

	string Unescape(string const & s)
	{
		string ret;
		ret.reserve(s.size());
		auto is_escaped = false;
		for (auto c : s)
		{
			if (is_escaped)
			{
				is_escaped = false;
				switch (c)
				{
				case 'n': ret += '\n'; break;
				case 'r': ret += '\r'; break;
				case 't': ret += '\t'; break;
				case '"': ret += '\"'; break;
				case '\\': ret += '\\'; break;
				default: ret += c;
				}
			}
			else if (c == '\\')
			{
				is_escaped = true;
			}
			else
			{
				ret += c;
			}
		}
		return ret;
	}
}
