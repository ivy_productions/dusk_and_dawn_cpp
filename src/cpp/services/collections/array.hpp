#pragma once

#include "iterators.hpp"
#include "slice.hpp"
#include <types/types.hpp>

using namespace gid_tech::types;

namespace gid_tech::collections
{
	template<typename T, usize TCount>
	class Array final
	{
	public:
		T elements[TCount];

	public:
		Array & operator=(Array<T, TCount> const & other) = default;

		T & operator[](usize index);
		T const & operator[](usize index) const;

		Slice<T> operator[](Range range);
		Slice<T const> operator[](Range range) const;

		b8 operator==(Array<T, TCount> const & other) const;
		b8 operator!=(Array<T, TCount> const & other) const;

		constexpr usize Count() const;
		constexpr b8 IsEmpty() const;

		T & First();
		T const & First() const;
		T & Last();
		T const & Last() const;

		IteratorForward<T> Begin();
		IteratorForward<T const> Begin() const;
		IteratorForward<T> End();
		IteratorForward<T const> End() const;

		IteratorBackward<T> RBegin();
		IteratorBackward<T const> RBegin() const;
		IteratorBackward<T> REnd();
		IteratorBackward<T const> REnd() const;
	};

	template<typename T, usize TCount>
	inline IteratorForward<T> begin(Array<T, TCount> & array)
	{
		return array.Begin();
	}

	template<typename T, usize TCount>
	inline IteratorForward<T const> begin(Array<T, TCount> const & array)
	{
		return array.Begin();
	}

	template<typename T, usize TCount>
	inline IteratorForward<T> end(Array<T, TCount> & array)
	{
		return array.End();
	}

	template<typename T, usize TCount>
	inline IteratorForward<T const> end(Array<T, TCount> const & array)
	{
		return array.End();
	}
}

#include "array.impl.hpp"
