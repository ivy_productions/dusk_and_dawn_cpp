#include "basic.hpp"

#include <math/constants.hpp>
#include <math/math.hpp>

namespace gid_tech::formatting
{
	void Parse::Write(c8 c)
	{
		chars[length] = c;
		length += 1;
		chars[length] = '\0';
	}

	void Parse::Write(c8 const * str)
	{
		usize i{};
		for (; str[i] != '\0'; i++)
			chars[length + i] = str[i];
		length += i;
		chars[length] = '\0';
	}

	void Parse::SetLenght(usize len)
	{
		length = len;
		chars[len] = '\0';
	}

	static void Reverse(Parse & p)
	{
		usize start{0};
		usize end{p.length - 1};

		while (start < end)
		{
			auto tmp = p.chars[start];
			p.chars[start] = p.chars[end];
			p.chars[end] = tmp;
			start++;
			end--;
		}
	}

	Parse FromB8(b8 v)
	{
		Parse ret{};
		if (v)
			ret.Write("true");
		else
			ret.Write("false");
		return ret;
	}

	Parse FromU64(u64 num, usize base)
	{
		Parse ret{};

		if (num == 0)
		{
			ret.Write("0");
			return ret;
		}

		while (num != 0)
		{
			auto rem = num % base;
			ret.Write((rem > 9) ? (rem - 10) + 'a' : rem + '0');
			num = num / base;
		}

		Reverse(ret);
		return ret;
	}

	Parse FromI64(i64 num, usize base)
	{
		Parse ret{};
		b8 is_negative = false;

		if (num == 0)
		{
			ret.Write("0");
			return ret;
		}

		if (num < 0 && base == 10)
		{
			is_negative = true;
			num = -num;
		}

		while (num != 0)
		{
			auto rem = num % base;
			ret.Write((rem > 9) ? (rem - 10) + 'a' : rem + '0');
			num = num / base;
		}

		if (is_negative)
			ret.Write('-');

		Reverse(ret);
		return ret;
	}

	// dtoa based on https://github.com/miloyip/dtoa-benchmark/blob/master/src/milo/dtoa_milo.h

	constexpr inline u64 U64C2(u64 h, u64 l)
	{
		return (static_cast<u64>(h) << 32) | static_cast<u64>(l);
	}

	struct F64Data final
	{
		u64 f{};
		i64 e{};

		static constexpr i64 diy_significand_size = 64;
		static constexpr i64 dp_significand_size = 52;
		static constexpr i64 dp_exponent_bias = 0x3FF + dp_significand_size;
		static constexpr i64 dp_min_exponent = -dp_exponent_bias;
		static constexpr u64 dp_exponent_mask = U64C2(0x7FF00000, 0x00000000);
		static constexpr u64 dp_significand_mask = U64C2(0x000FFFFF, 0xFFFFFFFF);
		static constexpr u64 dp_hidden_bit = U64C2(0x00100000, 0x00000000);

		F64Data(u64 f, i64 e) :
			f(f), e(e)
		{
		}

		F64Data(f64 d)
		{
			union
			{
				f64 d;
				u64 u;
			} u = {d};

			u64 biased_e = (u.u & dp_exponent_mask) >> dp_significand_size;
			u64 significand = (u.u & dp_significand_mask);
			if (biased_e != 0)
			{
				f = significand + dp_hidden_bit;
				e = biased_e - dp_exponent_bias;
			}
			else
			{
				f = significand;
				e = dp_min_exponent + 1;
			}
		}

		F64Data operator-(F64Data const & rhs) const
		{
			// assert(e == rhs.e);
			// assert(f >= rhs.f);
			return F64Data{f - rhs.f, e};
		}

		F64Data operator*(F64Data const & rhs) const
		{
			u64 const M32 = 0xFFFFFFFF;
			u64 const a = f >> 32;
			u64 const b = f & M32;
			u64 const c = rhs.f >> 32;
			u64 const d = rhs.f & M32;
			u64 const ac = a * c;
			u64 const bc = b * c;
			u64 const ad = a * d;
			u64 const bd = b * d;
			u64 tmp = (bd >> 32) + (ad & M32) + (bc & M32);
			tmp += 1U << 31; /// mult_round
			return F64Data{ac + (ad >> 32) + (bc >> 32) + (tmp >> 32), e + rhs.e + 64};
		}

		F64Data Normalize() const
		{
			auto ret = *this;
			while (!(ret.f & dp_hidden_bit))
			{
				ret.f <<= 1;
				ret.e -= 1;
			}
			ret.f <<= (diy_significand_size - dp_significand_size - 1);
			ret.e = ret.e - (diy_significand_size - dp_significand_size - 1);
			return ret;
		}

		F64Data NormalizeBoundary() const
		{
			F64Data ret = *this;
			while (!(ret.f & (dp_hidden_bit << 1)))
			{
				ret.f <<= 1;
				ret.e -= 1;
			}
			ret.f <<= (diy_significand_size - dp_significand_size - 2);
			ret.e = ret.e - (diy_significand_size - dp_significand_size - 2);
			return ret;
		}

		Array<F64Data, 2> NormalizedBoundaries() const
		{
			auto pl = F64Data((f << 1) + 1, e - 1).NormalizeBoundary();
			auto mi = (f == dp_hidden_bit) ? F64Data{(f << 2) - 1, e - 2} : F64Data{(f << 1) - 1, e - 1};
			mi.f <<= mi.e - pl.e;
			mi.e = pl.e;
			return {mi, pl};
		}
	};

	inline F64Data GetCachedPower(i64 e, i64 * K)
	{
		// 10^-348, 10^-340, ..., 10^340
		static const u64 cached_powers_f[] = {
			U64C2(0xfa8fd5a0, 0x081c0288), U64C2(0xbaaee17f, 0xa23ebf76),
			U64C2(0x8b16fb20, 0x3055ac76), U64C2(0xcf42894a, 0x5dce35ea),
			U64C2(0x9a6bb0aa, 0x55653b2d), U64C2(0xe61acf03, 0x3d1a45df),
			U64C2(0xab70fe17, 0xc79ac6ca), U64C2(0xff77b1fc, 0xbebcdc4f),
			U64C2(0xbe5691ef, 0x416bd60c), U64C2(0x8dd01fad, 0x907ffc3c),
			U64C2(0xd3515c28, 0x31559a83), U64C2(0x9d71ac8f, 0xada6c9b5),
			U64C2(0xea9c2277, 0x23ee8bcb), U64C2(0xaecc4991, 0x4078536d),
			U64C2(0x823c1279, 0x5db6ce57), U64C2(0xc2109436, 0x4dfb5637),
			U64C2(0x9096ea6f, 0x3848984f), U64C2(0xd77485cb, 0x25823ac7),
			U64C2(0xa086cfcd, 0x97bf97f4), U64C2(0xef340a98, 0x172aace5),
			U64C2(0xb23867fb, 0x2a35b28e), U64C2(0x84c8d4df, 0xd2c63f3b),
			U64C2(0xc5dd4427, 0x1ad3cdba), U64C2(0x936b9fce, 0xbb25c996),
			U64C2(0xdbac6c24, 0x7d62a584), U64C2(0xa3ab6658, 0x0d5fdaf6),
			U64C2(0xf3e2f893, 0xdec3f126), U64C2(0xb5b5ada8, 0xaaff80b8),
			U64C2(0x87625f05, 0x6c7c4a8b), U64C2(0xc9bcff60, 0x34c13053),
			U64C2(0x964e858c, 0x91ba2655), U64C2(0xdff97724, 0x70297ebd),
			U64C2(0xa6dfbd9f, 0xb8e5b88f), U64C2(0xf8a95fcf, 0x88747d94),
			U64C2(0xb9447093, 0x8fa89bcf), U64C2(0x8a08f0f8, 0xbf0f156b),
			U64C2(0xcdb02555, 0x653131b6), U64C2(0x993fe2c6, 0xd07b7fac),
			U64C2(0xe45c10c4, 0x2a2b3b06), U64C2(0xaa242499, 0x697392d3),
			U64C2(0xfd87b5f2, 0x8300ca0e), U64C2(0xbce50864, 0x92111aeb),
			U64C2(0x8cbccc09, 0x6f5088cc), U64C2(0xd1b71758, 0xe219652c),
			U64C2(0x9c400000, 0x00000000), U64C2(0xe8d4a510, 0x00000000),
			U64C2(0xad78ebc5, 0xac620000), U64C2(0x813f3978, 0xf8940984),
			U64C2(0xc097ce7b, 0xc90715b3), U64C2(0x8f7e32ce, 0x7bea5c70),
			U64C2(0xd5d238a4, 0xabe98068), U64C2(0x9f4f2726, 0x179a2245),
			U64C2(0xed63a231, 0xd4c4fb27), U64C2(0xb0de6538, 0x8cc8ada8),
			U64C2(0x83c7088e, 0x1aab65db), U64C2(0xc45d1df9, 0x42711d9a),
			U64C2(0x924d692c, 0xa61be758), U64C2(0xda01ee64, 0x1a708dea),
			U64C2(0xa26da399, 0x9aef774a), U64C2(0xf209787b, 0xb47d6b85),
			U64C2(0xb454e4a1, 0x79dd1877), U64C2(0x865b8692, 0x5b9bc5c2),
			U64C2(0xc83553c5, 0xc8965d3d), U64C2(0x952ab45c, 0xfa97a0b3),
			U64C2(0xde469fbd, 0x99a05fe3), U64C2(0xa59bc234, 0xdb398c25),
			U64C2(0xf6c69a72, 0xa3989f5c), U64C2(0xb7dcbf53, 0x54e9bece),
			U64C2(0x88fcf317, 0xf22241e2), U64C2(0xcc20ce9b, 0xd35c78a5),
			U64C2(0x98165af3, 0x7b2153df), U64C2(0xe2a0b5dc, 0x971f303a),
			U64C2(0xa8d9d153, 0x5ce3b396), U64C2(0xfb9b7cd9, 0xa4a7443c),
			U64C2(0xbb764c4c, 0xa7a44410), U64C2(0x8bab8eef, 0xb6409c1a),
			U64C2(0xd01fef10, 0xa657842c), U64C2(0x9b10a4e5, 0xe9913129),
			U64C2(0xe7109bfb, 0xa19c0c9d), U64C2(0xac2820d9, 0x623bf429),
			U64C2(0x80444b5e, 0x7aa7cf85), U64C2(0xbf21e440, 0x03acdd2d),
			U64C2(0x8e679c2f, 0x5e44ff8f), U64C2(0xd433179d, 0x9c8cb841),
			U64C2(0x9e19db92, 0xb4e31ba9), U64C2(0xeb96bf6e, 0xbadf77d9),
			U64C2(0xaf87023b, 0x9bf0ee6b)};

		static const i16 cached_powers_e[] = {
			-1220, -1193, -1166, -1140, -1113, -1087, -1060, -1034, -1007, -980,
			-954, -927, -901, -874, -847, -821, -794, -768, -741, -715,
			-688, -661, -635, -608, -582, -555, -529, -502, -475, -449,
			-422, -396, -369, -343, -316, -289, -263, -236, -210, -183,
			-157, -130, -103, -77, -50, -24, 3, 30, 56, 83,
			109, 136, 162, 189, 216, 242, 269, 295, 322, 348,
			375, 402, 428, 455, 481, 508, 534, 561, 588, 614,
			641, 667, 694, 720, 747, 774, 800, 827, 853, 880,
			907, 933, 960, 986, 1013, 1039, 1066};

		//int k = static_cast<int>(ceil((-61 - e) * 0.30102999566398114)) + 374;
		f64 dk = (-61 - e) * 0.30102999566398114 + 347; // dk must be positive, so can do ceiling in positive
		i64 k = static_cast<i64>(dk);
		if (dk - k > 0.0)
			k += 1;

		usize index = static_cast<usize>((k >> 3) + 1);
		*K = -(-348 + static_cast<isize>(index << 3)); // decimal exponent no need lookup table

		// assert(index < sizeof(cached_powers_f) / sizeof(cached_powers_f[0]));
		return F64Data{cached_powers_f[index], cached_powers_e[index]};
	}

	inline void GrisuRound(Parse & parse, u64 delta, u64 rest, u64 ten_kappa, u64 wp_w)
	{
		while (rest < wp_w && delta - rest >= ten_kappa &&
			   (rest + ten_kappa < wp_w || // closer
				wp_w - rest > rest + ten_kappa - wp_w))
		{
			parse.chars[parse.length - 1] -= 1; // buffer[len - 1]--;
			rest += ten_kappa;
		}
	}

	inline usize CountDecimalDigit(usize n)
	{
		// Simple pure C++ implementation was faster than __builtin_clz version in this situation.
		if (n < 10)
			return 1;
		if (n < 100)
			return 2;
		if (n < 1000)
			return 3;
		if (n < 10000)
			return 4;
		if (n < 100000)
			return 5;
		if (n < 1000000)
			return 6;
		if (n < 10000000)
			return 7;
		if (n < 100000000)
			return 8;
		if (n < 1000000000)
			return 9;
		return 10;
	}

	inline void DigitGen(F64Data const & W, F64Data const & Mp, u64 delta, Parse & parse, i64 * K)
	{
		static i32 const pow_10[] = {1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000};
		F64Data const one{u64(1) << -Mp.e, Mp.e};
		F64Data const wp_w = Mp - W;
		u32 p1 = static_cast<i64>(Mp.f >> -one.e);
		u64 p2 = Mp.f & (one.f - 1);
		i64 kappa = CountDecimalDigit(p1);

		while (kappa > 0)
		{
			u64 d;
			switch (kappa)
			{
			case 10:
				d = p1 / 1000000000;
				p1 %= 1000000000;
				break;
			case 9:
				d = p1 / 100000000;
				p1 %= 100000000;
				break;
			case 8:
				d = p1 / 10000000;
				p1 %= 10000000;
				break;
			case 7:
				d = p1 / 1000000;
				p1 %= 1000000;
				break;
			case 6:
				d = p1 / 100000;
				p1 %= 100000;
				break;
			case 5:
				d = p1 / 10000;
				p1 %= 10000;
				break;
			case 4:
				d = p1 / 1000;
				p1 %= 1000;
				break;
			case 3:
				d = p1 / 100;
				p1 %= 100;
				break;
			case 2:
				d = p1 / 10;
				p1 %= 10;
				break;
			case 1:
				d = p1;
				p1 = 0;
				break;
			default:
				__builtin_unreachable();
			}
			if (d || parse.length > 0)
				parse.Write('0' + static_cast<c8>(d));
			kappa--;
			u64 tmp = (static_cast<u64>(p1) << -one.e) + p2;
			if (tmp <= delta)
			{
				*K += kappa;
				GrisuRound(parse, delta, tmp, static_cast<u64>(pow_10[kappa]) << -one.e, wp_w.f);
				return;
			}
		}

		// when kappa == 0
		for (;;)
		{
			p2 *= 10;
			delta *= 10;
			c8 d = static_cast<c8>(p2 >> -one.e);
			if (d || parse.length > 0)
				parse.Write('0' + d);
			p2 &= one.f - 1;
			kappa--;
			if (p2 < delta)
			{
				*K += kappa;
				GrisuRound(parse, delta, p2, one.f, wp_w.f * pow_10[-kappa]);
				return;
			}
		}
	}

	inline void Grisu2(f64 value, Parse & parse, i64 * K)
	{
		F64Data const v(value);
		auto [w_m, w_p] = v.NormalizedBoundaries().elements;

		F64Data const c_mk = GetCachedPower(w_p.e, K);
		F64Data const W = v.Normalize() * c_mk;
		F64Data Wp = w_p * c_mk;
		F64Data Wm = w_m * c_mk;
		Wm.f++;
		Wp.f--;
		DigitGen(W, Wp, Wp.f - Wm.f, parse, K);
	}

	inline c8 const * GetDigitsLut()
	{
		static c8 const digits_lut[200] = {
			'0', '0', '0', '1', '0', '2', '0', '3', '0', '4', '0', '5', '0', '6', '0', '7', '0', '8', '0', '9',
			'1', '0', '1', '1', '1', '2', '1', '3', '1', '4', '1', '5', '1', '6', '1', '7', '1', '8', '1', '9',
			'2', '0', '2', '1', '2', '2', '2', '3', '2', '4', '2', '5', '2', '6', '2', '7', '2', '8', '2', '9',
			'3', '0', '3', '1', '3', '2', '3', '3', '3', '4', '3', '5', '3', '6', '3', '7', '3', '8', '3', '9',
			'4', '0', '4', '1', '4', '2', '4', '3', '4', '4', '4', '5', '4', '6', '4', '7', '4', '8', '4', '9',
			'5', '0', '5', '1', '5', '2', '5', '3', '5', '4', '5', '5', '5', '6', '5', '7', '5', '8', '5', '9',
			'6', '0', '6', '1', '6', '2', '6', '3', '6', '4', '6', '5', '6', '6', '6', '7', '6', '8', '6', '9',
			'7', '0', '7', '1', '7', '2', '7', '3', '7', '4', '7', '5', '7', '6', '7', '7', '7', '8', '7', '9',
			'8', '0', '8', '1', '8', '2', '8', '3', '8', '4', '8', '5', '8', '6', '8', '7', '8', '8', '8', '9',
			'9', '0', '9', '1', '9', '2', '9', '3', '9', '4', '9', '5', '9', '6', '9', '7', '9', '8', '9', '9'};
		return digits_lut;
	}

	inline void WriteExponent(Parse & parse, usize k)
	{
		if (k < 0)
		{
			parse.Write('-');
			k = -k;
		}

		if (k >= 100)
		{
			parse.Write('0' + static_cast<c8>(k / 100));
			k %= 100;
			c8 const * d = GetDigitsLut() + k * 2;
			parse.Write(d[0]);
			parse.Write(d[1]);
		}
		else if (k >= 10)
		{
			c8 const * d = GetDigitsLut() + k * 2;
			parse.Write(d[0]);
			parse.Write(d[1]);
		}
		else
		{
			parse.Write('0' + static_cast<c8>(k));
		}
	}

	inline void Prettify(Parse & parse, i64 k)
	{
		i64 const kk = k + parse.length; // 10^(kk-1) <= v < 10^kk

		if (kk > static_cast<i64>(parse.length) && kk <= 21)
		{
			// 1234e7 -> 12340000000
			for (usize i = parse.length; i < kk; i++)
				parse.Write('0');
			parse.Write(".0");
		}
		else if (kk >= 0 && kk <= 21)
		{
			// 1234e-2 -> 12.34
			auto tmp = parse;
			parse.SetLenght(kk);
			parse.Write('.');
			parse.Write(&tmp.chars[kk]);
		}
		else if (kk >= -6 && kk <= 0)
		{
			// 1234e-6 -> 0.001234
			auto tmp = parse;
			parse.SetLenght(0);
			parse.Write("0.");
			usize const offset = 2 - kk;
			for (usize i = 2; i < offset; i++)
				parse.Write('0');
			parse.Write(&tmp.chars[0]);
		}
		else if (parse.length == 1)
		{
			// 1e30
			parse.Write('e');
			WriteExponent(parse, kk - 1);
		}
		else
		{
			// 1234e30 -> 1.234e33
			auto tmp = parse;
			parse.SetLenght(1);
			parse.Write('.');
			parse.Write(&tmp.chars[1]);
			parse.Write('e');
			WriteExponent(parse, kk - 1);
		}
	}

	Parse FromF64(f64 v)
	{
		Parse ret;
		if (v == 0)
		{
			ret.Write("0.0");
		}
		else if (v == math::infinity)
		{
			ret.Write("Inf");
		}
		else if (v == math::negative_infinity)
		{
			ret.Write("-Inf");
		}
		else if (math::IsNaN(v))
		{
			ret.Write("NaN");
		}
		else
		{
			i64 k{};
			Grisu2(v > 0 ? v : -v, ret, &k);
			Prettify(ret, k);

			if (v < 0)
			{
				auto tmp = ret;
				ret.SetLenght(0);
				ret.Write('-');
				ret.Write(&tmp.chars[0]);
			}
		}
		return ret;
	}
}
