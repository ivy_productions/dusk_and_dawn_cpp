#include "file_watcher.hpp"

#include <iostream>

namespace gid_tech::file_watch
{
	FileWatcher::FileWatcher(string path) :
		tracked_path(path)
	{
	}

	map<string, FileState> FileWatcher::Update()
	{
		map<string, FileState> ret;
		for (auto & directory_entry : directory_iterator(tracked_path))
		{
			file_time_type mod_time;

			error_code error;
			mod_time = directory_entry.last_write_time(error);
			if (error)
			{
				cout << error << "file watch race. skipping and trying again" << endl;
				continue;
			}

			string path = directory_entry.path().string();
			if (tracked_files.find(path) == tracked_files.end())
			{
				tracked_files[path] = mod_time;
				ret[path] = FileState::Added;
			}
			if ((mod_time - tracked_files[path]) > chrono::milliseconds(20))
			{
				tracked_files[path] = mod_time;
				ret[path] = FileState::Modified;
			}
		}

		auto tracked_files_copy(tracked_files);
		for (auto & [tf, _] : tracked_files_copy)
		{
			if (!is_regular_file(tf))
			{
				ret[tf] = FileState::Removed;
				tracked_files.erase(tf);
			}
		}
		return ret;
	}
}
