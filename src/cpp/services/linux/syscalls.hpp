#pragma once

#ifdef LINUX

#include <types/types.hpp>

using namespace gid_tech::types;

namespace gid_tech::linux
{
	usize Syscall(usize number);
	usize Syscall(usize number, usize arg1);
	usize Syscall(usize number, usize arg1, usize arg2);
	usize Syscall(usize number, usize arg1, usize arg2, usize arg3);
	usize Syscall(usize number, usize arg1, usize arg2, usize arg3, usize arg4);
	usize Syscall(usize number, usize arg1, usize arg2, usize arg3, usize arg4, usize arg5);
	usize Syscall(usize number, usize arg1, usize arg2, usize arg3, usize arg4, usize arg5, usize arg6);

	// syscalls based off https://filippo.io/linux-syscall-table/
	inline usize Read(usize fd, u8 const * buf, usize count)
	{
		return Syscall(0, fd, (usize) buf, count);
	}
	inline usize Write(usize fd, u8 const * buf, usize count)
	{
		return Syscall(1, fd, (usize) buf, count);
	}
	inline usize Open(u8 const * file_name, usize flags, usize mode)
	{
		return Syscall(2, (usize) file_name, flags, mode);
	}
	inline usize Close(usize fd, u8 const * buf, usize count)
	{
		return Syscall(3, fd, (usize) buf, count);
	}

	inline usize MMap(usize addr, usize len, usize prot, usize flags, usize fd, usize off)
	{
		return Syscall(9, addr, len, prot, flags, fd, off);
	}
	inline usize MUnmap(usize addr, usize len)
	{
		return Syscall(11, addr, len);
	}

	[[noreturn]] inline void Exit(usize code)
	{
		Syscall(231, code);
		__builtin_unreachable();
	}
}

#endif
