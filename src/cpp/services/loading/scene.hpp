#pragma once

#include <filesystem>
#include <functional>
#include <memory>

#include <rendering/render_manager.hpp>
#include <rendering/resources/image.hpp>
#include <rendering/resources/pipeline.hpp>
#include <rendering/targets/frame.hpp>

#include "material_data.hpp"
#include "scene_elements.hpp"

namespace gid_tech::loading::scene
{
	using namespace std;
	using namespace filesystem;
	using namespace rendering;

	shared_ptr<SceneElements> Load(
		path path,
		RenderManager & render_manager,

		function<shared_ptr<Pipeline>(string const &, MaterialData const &)> pipeline_selector,
		function<shared_ptr<Frame>(string const &)> frame_selector,
		function<shared_ptr<Image>(filesystem::path const &)> image_selector);
}
