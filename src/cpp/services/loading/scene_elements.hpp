#pragma once

#include <tuple>
#include <memory>

#include <scene/node.hpp>
#include <rendering/resources/mesh.hpp>
#include <rendering/resources/material.hpp>

namespace gid_tech::loading
{
	using namespace scene;
	using namespace rendering;

	using SceneElements = tuple<
		vector<shared_ptr<Node>>,
		vector<shared_ptr<Mesh>>,
		vector<shared_ptr<Material>>>;
}
