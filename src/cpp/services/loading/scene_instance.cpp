#include "scene_instance.hpp"

namespace gid_tech::loading
{
	SceneInstance InstantiateSceneElements(shared_ptr<SceneElements> elements, Optional<variant<Scene *, Node *>> scene_or_parent)
	{
		vector<Node *> root_node_instances;
		vector<shared_ptr<Node>> node_instances;
		auto & [nodes, ___, ____] = *elements;
		for (auto & node : nodes)
		{
			if (node->GetParent().IsNone())
			{
				auto [root_node, all_nodes] = node->MakeCopy(scene_or_parent);
				root_node_instances.emplace_back(root_node);
				node_instances.insert(node_instances.end(), all_nodes.begin(), all_nodes.end());
			}
		}
		return make_tuple(move(root_node_instances), move(node_instances), elements);
	}
}
