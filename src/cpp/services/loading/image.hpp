#pragma once

#include <filesystem>
#include <functional>
#include <memory>

#include <rendering/resources/image.hpp>

namespace gid_tech::loading::image
{
	using namespace std;
	using namespace filesystem;
	using namespace rendering;

	shared_ptr<Image> Load(path path, RenderManager & render_manager);
}
