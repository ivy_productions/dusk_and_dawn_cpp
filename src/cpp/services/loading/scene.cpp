#include "scene.hpp"

#include <fstream>

#include <algebra/transform.hpp>
#include <scene/model_node.hpp>

#include <object_model/object_model.hpp>
#include <object_model_json/json.hpp>

namespace gid_tech::loading::scene
{
	using namespace object_model;
	using namespace algebra;

	shared_ptr<SceneElements> Load(
		path path,
		RenderManager & render_manager,

		function<shared_ptr<Pipeline>(string const &, MaterialData const &)> pipeline_selector,
		function<shared_ptr<Frame>(string const &)> frame_selector,
		function<shared_ptr<Image>(filesystem::path const &)> image_selector)
	{
		assert(path.extension() == ".gltf");
		ifstream file(path, ios::binary);
		string gltf_string((istreambuf_iterator<char>(file)), istreambuf_iterator<char>());
		auto gltf = json::JsonToObject(gltf_string).value();

		auto * gltf_buffers = gltf.GetObjectValue("buffers").value();
		auto gltf_textures = gltf.GetObjectValue("textures");
		auto gltf_images = gltf.GetObjectValue("images");
		auto * gltf_materials = gltf.GetObjectValue("materials").value();
		auto * gltf_meshes = gltf.GetObjectValue("meshes").value();
		auto * gltf_buffer_views = gltf.GetObjectValue("bufferViews").value();
		auto * gltf_accessors = gltf.GetObjectValue("accessors").value();
		auto * gltf_scenes = gltf.GetObjectValue("scenes").value();
		auto * gltf_nodes = gltf.GetObjectValue("nodes").value();

		// bin loading
		vector<vector<u8>> buffers;
		auto buffer_count = gltf_buffers->GetArrayCount().value();
		for (usize buffer_index = 0; buffer_index < buffer_count; buffer_index++)
		{
			auto * gltf_buffer = gltf_buffers->GetArrayValue(buffer_index).value();
			auto uri = gltf_buffer->GetObjectValue("uri").value()->AsString().value();
			auto dir = path.parent_path();
			auto buffer_path = dir.append(uri);
			ifstream buffer_file(buffer_path, ios::binary);
			buffers.emplace_back((istreambuf_iterator<char>(buffer_file)), istreambuf_iterator<char>());
		}

		// image loading
		vector<shared_ptr<Image>> images;
		if (gltf_images.has_value())
		{
			auto texture_count = gltf_textures.value()->GetArrayCount().value();
			for (usize texture_index = 0; texture_index < texture_count; texture_index++)
			{
				auto * gltf_texture = gltf_textures.value()->GetArrayValue(texture_index).value();
				auto image_index = (usize) gltf_texture->GetObjectValue("source").value()->AsUInt().value();
				auto * gltf_image = gltf_images.value()->GetArrayValue(image_index).value();
				auto uri = gltf_image->GetObjectValue("uri").value()->AsString().value();
				auto dir = path.parent_path();
				auto image_path = dir.append(uri);

				images.emplace_back(image_selector(image_path));
			}
		}

		// material loading
		vector<shared_ptr<Material>> materials;
		vector<vector<shared_ptr<Image>>> material_images;
		vector<string> material_names;
		auto material_count = gltf_materials->GetArrayCount().value();
		for (usize material_index = 0; material_index < material_count; material_index++)
		{
			auto * gltf_material = gltf_materials->GetArrayValue(material_index).value();
			auto name = gltf_material->GetObjectValue("name").value()->AsString().value();
			material_names.emplace_back(name);

			auto * gltf_material_data = gltf_material->GetObjectValue("pbrMetallicRoughness").value();

			vector<u32> material_data_data(sizeof(MaterialData) / sizeof(u32));
			auto material_data = new (material_data_data.data()) MaterialData{};

			auto gltf_color = gltf_material_data->GetObjectValue("baseColorFactor");
			if (gltf_color.has_value())
			{
				material_data->color = {
					gltf_color.value()->GetArrayValue(0).value()->AsFloat().value(),
					gltf_color.value()->GetArrayValue(1).value()->AsFloat().value(),
					gltf_color.value()->GetArrayValue(2).value()->AsFloat().value(),
					gltf_color.value()->GetArrayValue(3).value()->AsFloat().value(),
				};
			}
			auto gltf_metalicness = gltf_material_data->GetObjectValue("metallicFactor");
			if (gltf_metalicness.has_value())
				material_data->metalicness = gltf_metalicness.value()->AsFloat().value();
			auto gltf_roughness = gltf_material_data->GetObjectValue("roughnessFactor");
			if (gltf_roughness.has_value())
				material_data->roughness = gltf_roughness.value()->AsFloat().value();

			auto material = make_shared<Material>(render_manager, move(material_data_data));
			materials.emplace_back(material);

			// material images
			vector<shared_ptr<Image>> m_images;

			auto gltf_color_image = gltf_material_data->GetObjectValue("baseColorTexture");
			if (gltf_color_image.has_value())
			{
				auto color_image_index = gltf_color_image.value()->GetObjectValue("index").value()->AsUInt().value();
				m_images.emplace_back(images[color_image_index]);
			}
			else
				m_images.emplace_back(image_selector("images/default/white.png"));

			auto gltf_mr_image = gltf_material_data->GetObjectValue("metallicRoughnessTexture");
			if (gltf_mr_image.has_value())
			{
				auto mr_image_index = gltf_mr_image.value()->GetObjectValue("index").value()->AsUInt().value();
				m_images.emplace_back(images[mr_image_index]);
			}
			else
				m_images.emplace_back(image_selector("images/default/white.png"));

			auto gltf_normal_image = gltf_material->GetObjectValue("normalTexture");
			if (gltf_normal_image.has_value())
			{
				auto normal_image_index = gltf_normal_image.value()->GetObjectValue("index").value()->AsUInt().value();
				m_images.emplace_back(images[normal_image_index]);
			}
			else
				m_images.emplace_back(image_selector("images/default/normal.png"));

			material_images.emplace_back(move(m_images));
		}

		// mesh loading
		vector<shared_ptr<Mesh>> meshes;
		vector<vector<tuple<shared_ptr<Mesh>, usize>>> meshes_and_materials;
		usize mesh_count = gltf_meshes->GetArrayCount().value();
		for (usize mesh_index = 0; mesh_index < mesh_count; mesh_index++)
		{
			auto * gltf_mesh = gltf_meshes->GetArrayValue(mesh_index).value();
			auto * gltf_primitives = gltf_mesh->GetObjectValue("primitives").value();
			auto primitives_count = gltf_primitives->GetArrayCount().value();
			auto & mesh_primitives = meshes_and_materials.emplace_back();
			for (usize primitive_index = 0; primitive_index < primitives_count; primitive_index++)
			{
				auto * gltf_primitive = gltf_primitives->GetArrayValue(primitive_index).value();
				auto * gltf_attributes = gltf_primitive->GetObjectValue("attributes").value();
				auto material_index = gltf_primitive->GetObjectValue("material").value()->AsUInt().value();

				auto indices_index = gltf_primitive->GetObjectValue("indices").value()->AsUInt().value();

				auto position_index = gltf_attributes->GetObjectValue("POSITION").value()->AsUInt().value();
				auto normal_index = gltf_attributes->GetObjectValue("NORMAL").value()->AsUInt().value();
				auto tangent_index = gltf_attributes->GetObjectValue("TANGENT").value()->AsUInt().value();
				auto uv_index = gltf_attributes->GetObjectValue("TEXCOORD_0").value()->AsUInt().value();

				// index
				vector<data::Index> indices;

				auto * gltf_indices_accessor = gltf_accessors->GetArrayValue(indices_index).value();
				auto * gltf_indices_buffer_view =
					gltf_buffer_views->GetArrayValue(gltf_indices_accessor->GetObjectValue("bufferView").value()->AsUInt().value()).value();
				auto index_count = gltf_indices_accessor->GetObjectValue("count").value()->AsUInt().value();
				auto index_buffer_index = gltf_indices_buffer_view->GetObjectValue("buffer").value()->AsUInt().value();
				auto index_offset = gltf_indices_buffer_view->GetObjectValue("byteOffset").value()->AsUInt().value();
				auto index_size = gltf_indices_buffer_view->GetObjectValue("byteLength").value()->AsUInt().value();
				if (index_size / index_count == 2)
				{
					auto index_data = (Vector<u16, 3> *) &buffers[index_buffer_index][index_offset];
					for (usize i = 0; i < index_count / 3; i++)
						indices.emplace_back(data::Index{{index_data[i].x, index_data[i].y, index_data[i].z}});
				}
				else if (index_size / index_count == 4)
				{
					auto index_data = (Vector<u32, 3> *) &buffers[index_buffer_index][index_offset];
					for (usize i = 0; i < index_count / 3; i++)
						indices.emplace_back(data::Index{index_data[i]});
				}

				// vertex
				vector<data::Vertex> vertices;

				// position
				auto * gltf_position_accessor = gltf_accessors->GetArrayValue(position_index).value();
				auto * gltf_position_buffer_view =
					gltf_buffer_views->GetArrayValue(gltf_position_accessor->GetObjectValue("bufferView").value()->AsUInt().value()).value();
				auto position_count = gltf_position_accessor->GetObjectValue("count").value()->AsUInt().value();
				auto position_buffer_index = gltf_position_buffer_view->GetObjectValue("buffer").value()->AsUInt().value();
				auto position_offset = gltf_position_buffer_view->GetObjectValue("byteOffset").value()->AsUInt().value();
				auto position_data = (Vector<f32, 3> *) &buffers[position_buffer_index][position_offset];
				for (usize i = 0; i < position_count; i++)
					vertices.emplace_back(data::Vertex{{position_data[i]}});

				// normal
				auto gltf_normal_accessor = gltf_accessors->GetArrayValue(normal_index);
				if (gltf_normal_accessor.has_value())
				{
					auto * gltf_normal_buffer_view =
						gltf_buffer_views->GetArrayValue(gltf_normal_accessor.value()->GetObjectValue("bufferView").value()->AsUInt().value()).value();
					auto normal_count = gltf_normal_accessor.value()->GetObjectValue("count").value()->AsUInt().value();
					auto normal_buffer_index = gltf_normal_buffer_view->GetObjectValue("buffer").value()->AsUInt().value();
					auto normal_offset = gltf_normal_buffer_view->GetObjectValue("byteOffset").value()->AsUInt().value();
					auto normal_data = (Vector<f32, 3> *) &buffers[normal_buffer_index][normal_offset];
					for (usize i = 0; i < normal_count; i++)
						vertices[i].normal = normal_data[i];
				}

				// tangent
				auto gltf_tangent_accessor = gltf_accessors->GetArrayValue(tangent_index);
				if (gltf_tangent_accessor.has_value())
				{
					auto * gltf_tangent_buffer_view =
						gltf_buffer_views->GetArrayValue(gltf_tangent_accessor.value()->GetObjectValue("bufferView").value()->AsUInt().value()).value();
					auto tangent_count = gltf_tangent_accessor.value()->GetObjectValue("count").value()->AsUInt().value();
					auto tangent_buffer_index = gltf_tangent_buffer_view->GetObjectValue("buffer").value()->AsUInt().value();
					auto tangent_offset = gltf_tangent_buffer_view->GetObjectValue("byteOffset").value()->AsUInt().value();
					auto tangent_data = (Vector<f32, 4> *) &buffers[tangent_buffer_index][tangent_offset];
					for (usize i = 0; i < tangent_count; i++)
					{
						vertices[i].tangent = tangent_data[i];
						vertices[i].bitangent = vertices[i].tangent.Cross(vertices[i].normal);
					}
				}

				// uv
				auto gltf_uv_accessor = gltf_accessors->GetArrayValue(uv_index);
				if (gltf_uv_accessor.has_value())
				{
					auto * gltf_uv_buffer_view =
						gltf_buffer_views->GetArrayValue(gltf_uv_accessor.value()->GetObjectValue("bufferView").value()->AsUInt().value()).value();
					auto uv_count = gltf_uv_accessor.value()->GetObjectValue("count").value()->AsUInt().value();
					auto uv_buffer_index = gltf_uv_buffer_view->GetObjectValue("buffer").value()->AsUInt().value();
					auto uv_offset = gltf_uv_buffer_view->GetObjectValue("byteOffset").value()->AsUInt().value();
					auto uv_data = (Vector<f32, 2> *) &buffers[uv_buffer_index][uv_offset];
					for (usize i = 0; i < uv_count; i++)
						vertices[i].uv = uv_data[i];
				}

				auto mesh = make_shared<Mesh>(render_manager, move(vertices), move(indices));
				mesh_primitives.emplace_back(mesh, material_index);
				meshes.emplace_back(mesh);
			}
		}


		// node loading
		vector<shared_ptr<Node>> nodes;
		auto scenes_count = gltf_scenes->GetArrayCount().value();
		for (usize scene_index = 0; scene_index < scenes_count; scene_index++)
		{
			auto gltf_scene = gltf_scenes->GetArrayValue(scene_index).value();
			auto * gltf_scene_nodes = gltf_scene->GetObjectValue("nodes").value();
			auto scene_node_count = gltf_scene_nodes->GetArrayCount().value();
			for (usize node_index = 0; node_index < scene_node_count; node_index++)
			{
				function<void(usize, optional<Node *>)> load_sub_node = [&](usize ni, optional<Node *> parent) {
					auto gltf_node = gltf_nodes->GetArrayValue(ni).value();
					auto name = gltf_node->GetObjectValue("name").value()->AsString().value();

					auto gltf_translation = gltf_node->GetObjectValue("translation");
					auto gltf_rotation = gltf_node->GetObjectValue("rotation");
					auto gltf_scale = gltf_node->GetObjectValue("scale");
					auto translation = gltf_translation.has_value() ?
						  Vector<f32, 3>{
							gltf_translation.value()->GetArrayValue(0).value()->AsFloat().value(),
							gltf_translation.value()->GetArrayValue(1).value()->AsFloat().value(),
							gltf_translation.value()->GetArrayValue(2).value()->AsFloat().value(),
						} :
						  Vector<f32, 3>{};
					auto rotation = gltf_rotation.has_value() ?
						  Quaternion<f32>{
							gltf_rotation.value()->GetArrayValue(0).value()->AsFloat().value(),
							gltf_rotation.value()->GetArrayValue(1).value()->AsFloat().value(),
							gltf_rotation.value()->GetArrayValue(2).value()->AsFloat().value(),
							gltf_rotation.value()->GetArrayValue(3).value()->AsFloat().value(),
						} :
						  Quaternion<f32>{};
					auto scale = gltf_scale.has_value() ?
						  Vector<f32, 3>{
							gltf_scale.value()->GetArrayValue(0).value()->AsFloat().value(),
							gltf_scale.value()->GetArrayValue(1).value()->AsFloat().value(),
							gltf_scale.value()->GetArrayValue(2).value()->AsFloat().value(),
						} :
						  Vector<f32, 3>{1, 1, 1};

					auto mesh = gltf_node->GetObjectValue("mesh");
					shared_ptr<Node> node;
					if (mesh.has_value())
					{
						auto mesh_index = mesh.value()->AsUInt().value();
						vector<ModelNode::SurfaceData> surfaces;
						for (auto & [mesh, material_index] : meshes_and_materials[mesh_index])
						{
							surfaces.emplace_back(ModelNode::SurfaceData{
								.name = material_names[material_index],
								.pipeline = pipeline_selector(material_names[material_index], *reinterpret_cast<MaterialData const *>(materials[material_index]->Get().data())),
								.mesh = mesh,
								.material = materials[material_index],
								.images = material_images[material_index],
							});
						}
						node = parent.has_value() ?
							  make_shared<ModelNode>(name, Node::Transform{translation, rotation, scale}, parent.value(), move(surfaces)) :
							  make_shared<ModelNode>(name, Node::Transform{translation, rotation, scale}, move(surfaces));
					}
					else
					{
						node = parent.has_value() ?
							  make_shared<Node>(name, Node::Transform{translation, rotation, scale}, parent.value()) :
							  make_shared<Node>(name, Node::Transform{translation, rotation, scale});
					}
					nodes.emplace_back(node);

					auto children = gltf_node->GetObjectValue("children");
					if (children.has_value())
					{
						auto c = children.value()->GetArrayCount().value();
						for (usize ci = 0; ci < c; ci++)
						{
							usize child_index = children.value()->GetArrayValue(ci).value()->AsUInt().value();
							load_sub_node(child_index, make_optional(node.get()));
						}
					}
				};

				usize sub_node_index = gltf_scene_nodes->GetArrayValue(node_index).value()->AsUInt().value();
				load_sub_node(sub_node_index, nullopt);
			}
		}
		return make_shared<SceneElements>(make_tuple(nodes, meshes, materials));
	}
}
