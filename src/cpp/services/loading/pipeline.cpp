#include "pipeline.hpp"

#include <fstream>

namespace gid_tech::loading::pipeline
{
	shared_ptr<Pipeline> Load(
		path path_vert,
		path path_frag,
		u32 image_count,
		bool is_transparent,
		RenderManager & render_manager)
	{
		ifstream vert_file(path_vert, ios::binary);
		ifstream frag_file(path_frag, ios::binary);
		vector<u8> vert_data((istreambuf_iterator<char>(vert_file)), istreambuf_iterator<char>());
		vector<u8> frag_data((istreambuf_iterator<char>(frag_file)), istreambuf_iterator<char>());

		return make_shared<Pipeline>(render_manager, image_count, is_transparent, move(vert_data), move(frag_data));
	}
}
