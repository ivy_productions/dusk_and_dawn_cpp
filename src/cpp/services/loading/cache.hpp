#pragma once

#include <filesystem>
#include <map>
#include <memory>
#include <tuple>

#include <loading/image.hpp>
#include <rendering/render_manager.hpp>
#include <rendering/resources/image.hpp>
#include <rendering/resources/material.hpp>
#include <rendering/resources/mesh.hpp>
#include <rendering/resources/pipeline.hpp>
#include <rendering/targets/frame.hpp>
#include <scene/node.hpp>

#include "material_data.hpp"
#include "scene_elements.hpp"

using namespace std;
using namespace filesystem;

namespace gid_tech::loading
{
	using namespace gid_tech::rendering;
	using namespace gid_tech::scene;
	using loading::SceneElements;

	class LoadingCache
	{
	private:
		RenderManager & render_manager;
		map<tuple<path, path, u32, bool>, weak_ptr<Pipeline>> pipelines;
		map<path, weak_ptr<Image>> images;
		map<path, weak_ptr<SceneElements>> scenes;

		function<shared_ptr<Pipeline>(string const &, MaterialData const &)> pipeline_selector;
		function<shared_ptr<Frame>(string const &)> frame_selector;

	public:
		LoadingCache(
			RenderManager & render_manager,
			function<shared_ptr<Pipeline>(string const &, MaterialData const &)> pipeline_selector,
			function<shared_ptr<Frame>(string const &)> frame_selector);

		shared_ptr<Pipeline> GetOrLoadPipeline(path path_vert, path path_frag, u32 image_count, bool is_transparent);
		shared_ptr<Image> GetOrLoadImage(path path);
		shared_ptr<SceneElements> GetOrLoadScene(path path);
	};
}
 