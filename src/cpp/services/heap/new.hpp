#pragma once

#include <types/types.hpp>

using namespace gid_tech::types;

void * operator new(usize size);
void * operator new[](usize size);

void * operator new(usize size, void * where) noexcept;
void * operator new[](usize size, void * where) noexcept;

void operator delete(void * ptr) noexcept;
void operator delete[](void * ptr) noexcept;
