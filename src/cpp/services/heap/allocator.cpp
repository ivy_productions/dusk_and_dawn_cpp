#include "allocator.hpp"

#include "raw_alloc.hpp"

namespace gid_tech::heap
{
	void * DefaultAlloc(void * data, u64 size)
	{
		return RawAlloc(size);
	}

	void DefaultDealloc(void * data, void * ptr)
	{
		RawDealloc(ptr);
	}

	usize DefaultSize(void * data, void * ptr)
	{
		return RawSize(ptr);
	}

	Allocator Allocator::default_allocator = Allocator{
		nullptr,
		DefaultAlloc,
		DefaultDealloc,
		DefaultSize,
	};
}
