#pragma once

#include <types/types.hpp>

using namespace gid_tech::types;

namespace gid_tech::heap
{
	class Allocator
	{
	private:
		void * const data;
		void * (*const alloc)(void * data, usize size);
		void (*const dealloc)(void * data, void * ptr);
		usize (*const size)(void * data, void * ptr);

	public:
		constexpr Allocator(
			void * const data,
			void * (*const alloc)(void * data, usize size),
			void (*const dealloc)(void * data, void * ptr),
			usize (*const size)(void * data, void * ptr)) :
			data(data),
			alloc(alloc),
			dealloc(dealloc),
			size(size)
		{
		}

		template<typename T>
		inline T * Alloc(usize count)
		{
			return static_cast<T *>(alloc(data, sizeof(T) * count));
		}

		template<typename T>
		inline void Dealloc(T * ptr)
		{
			dealloc(data, ptr);
		}

		template<typename T>
		inline usize Count(T * ptr)
		{
			return size(data, ptr) / sizeof(T);
		}

		static Allocator default_allocator;
	};
}
