#pragma once

#include <types/types.hpp>

using namespace gid_tech::types;

namespace gid_tech::heap
{
	void * RawAlloc(usize size);
	void RawDealloc(void * ptr);
	usize RawSize(void * ptr);
}
