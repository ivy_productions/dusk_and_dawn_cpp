#include "frame.hpp"

#include <collections/array.hpp>

#include "../../vulkan/render_manager.hpp"
#include "../frame.hpp"

using namespace gid_tech::collections;

namespace gid_tech::rendering
{
	Frame::Frame(RenderManager & render_manager, Size size) :
		FrameImplementation(render_manager.device, render_manager.frame_config, render_manager.memory_cache, VkExtent2D{size.width, size.height})
	{
	}

	Frame::~Frame()
	{
	}

	Size Frame::GetSize() const
	{
		auto extent = implementation::Frame::GetExtent();
		return {extent.width, extent.height};
	}

	void Frame::SetSize(Size size)
	{
		implementation::Frame::Resize(VkExtent2D{size.width, size.height});
	}
}

namespace gid_tech::rendering::implementation
{
	Frame::Frame(Device & device, FrameConfig const & frame_config, MemoryCache & memory_cache, VkExtent2D extent) :
		device(device),
		frame_config(frame_config),
		memory_cache(memory_cache),
		extent(extent),
		color_image{},
		depth_image{},
		color_image_view{},
		depth_image_view{},
		framebuffer{}
	{
		Set(extent);
	}

	Frame::~Frame()
	{
		Unset();
	}

	VkExtent2D Frame::GetExtent() const
	{
		return extent;
	}

	VkImage Frame::GetColorImage() const
	{
		return color_image;
	}

	VkImage Frame::GetDepthImage() const
	{
		return depth_image;
	}

	VkFramebuffer Frame::GetFramebuffer() const
	{
		return framebuffer;
	}

	void Frame::Resize(VkExtent2D extent)
	{
		Unset();
		Set(extent);
	}

	void Frame::Unset()
	{
		memory_cache.Unbind(color_image);
		memory_cache.Unbind(depth_image);

		vkDestroyImageView(device.GetDevice(), color_image_view, nullptr);
		vkDestroyImageView(device.GetDevice(), depth_image_view, nullptr);
		vkDestroyImage(device.GetDevice(), color_image, nullptr);
		vkDestroyImage(device.GetDevice(), depth_image, nullptr);
		vkDestroyFramebuffer(device.GetDevice(), framebuffer, nullptr);
	}

	void Frame::Set(VkExtent2D extent)
	{
		this->extent = extent;
		color_image = CreateImage(device.GetDevice(), device.GetQueueIndex(VK_QUEUE_GRAPHICS_BIT), extent, true);
		depth_image = CreateImage(device.GetDevice(), device.GetQueueIndex(VK_QUEUE_GRAPHICS_BIT), extent, false);
		memory_cache.Bind(color_image, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
		memory_cache.Bind(depth_image, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

		color_image_view = CreateImageView(device.GetDevice(), color_image, true);
		depth_image_view = CreateImageView(device.GetDevice(), depth_image, false);
		framebuffer = CreateFramebuffer(device.GetDevice(), frame_config.GetRenderPass(), color_image_view, depth_image_view, extent);
	}

	VkImage Frame::CreateImage(VkDevice device, u32 graphics_queue_index, VkExtent2D extent, bool is_color)
	{
		VkImageCreateInfo create_info{
			.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
			.imageType = VK_IMAGE_TYPE_2D,
			.format = is_color ?
				  VK_FORMAT_R32G32B32A32_SFLOAT :
				  VK_FORMAT_D32_SFLOAT,
			.extent = VkExtent3D{extent.width, extent.height, 1},
			.mipLevels = 1,
			.arrayLayers = 1,
			.samples = VK_SAMPLE_COUNT_1_BIT,
			.tiling = VK_IMAGE_TILING_OPTIMAL,
			.usage = is_color ?
				  VkBufferUsageFlags(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT) :
				  VkBufferUsageFlags(VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT),
			.sharingMode = VK_SHARING_MODE_EXCLUSIVE,
			.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
		};

		VkImage image{};
		vkCreateImage(device, &create_info, nullptr, &image);
		return image;
	}

	VkImageView Frame::CreateImageView(VkDevice device, VkImage image, bool is_color)
	{
		VkImageViewCreateInfo create_info{
			.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
			.image = image,
			.viewType = VK_IMAGE_VIEW_TYPE_2D,
			.format = is_color ?
				  VK_FORMAT_R32G32B32A32_SFLOAT :
				  VK_FORMAT_D32_SFLOAT,
			.subresourceRange = VkImageSubresourceRange{
				.aspectMask = is_color ?
					  VkImageAspectFlags(VK_IMAGE_ASPECT_COLOR_BIT) :
					  VkImageAspectFlags(VK_IMAGE_ASPECT_DEPTH_BIT),
				.baseMipLevel = 0,
				.levelCount = 1,
				.baseArrayLayer = 0,
				.layerCount = 1,
			},
		};

		VkImageView image_view{};
		vkCreateImageView(device, &create_info, nullptr, &image_view);
		return image_view;
	}

	VkFramebuffer Frame::CreateFramebuffer(VkDevice device, VkRenderPass render_pass, VkImageView color_image_view, VkImageView depth_image_view, VkExtent2D extent)
	{
		Array<VkImageView, 2> attachments{color_image_view, depth_image_view};

		VkFramebufferCreateInfo create_info{
			.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
			.renderPass = render_pass,
			.attachmentCount = static_cast<u32>(attachments.Count()),
			.pAttachments = attachments.elements,
			.width = extent.width,
			.height = extent.height,
			.layers = 1,
		};

		VkFramebuffer framebuffer{};
		vkCreateFramebuffer(device, &create_info, nullptr, &framebuffer);
		return framebuffer;
	}
}
