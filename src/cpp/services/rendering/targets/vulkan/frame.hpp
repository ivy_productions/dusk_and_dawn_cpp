#pragma once

#include <vulkan/vulkan.h>

#include "../../vulkan/device.hpp"
#include "../../vulkan/draw_configs/frame_config.hpp"
#include "../../vulkan/memory/memory_cache.hpp"

using namespace gid_tech::types;

namespace gid_tech::rendering::implementation
{
	class Frame
	{
	private:
		Device & device;
		FrameConfig const & frame_config;
		MemoryCache & memory_cache;

		VkExtent2D extent;

		VkImage color_image;
		VkImage depth_image;
		VkImageView color_image_view;
		VkImageView depth_image_view;
		VkFramebuffer framebuffer;

	public:
		Frame(Device & device, FrameConfig const & frame_config, MemoryCache & memory_cache, VkExtent2D extent);
		~Frame();

		Frame(Frame const &) = delete;
		Frame & operator=(Frame const &) = delete;


		VkExtent2D GetExtent() const;
		VkImage GetColorImage() const;
		VkImage GetDepthImage() const;
		VkFramebuffer GetFramebuffer() const;

		void Resize(VkExtent2D extent);

	private:
		void Unset();
		void Set(VkExtent2D extent);

		static VkImage CreateImage(VkDevice device, u32 graphics_queue_index, VkExtent2D extent, bool is_color);
		static VkImageView CreateImageView(VkDevice device, VkImage image, bool is_color);
		static VkFramebuffer CreateFramebuffer(VkDevice device, VkRenderPass render_pass, VkImageView color_image_view, VkImageView depth_image_view, VkExtent2D extent);
	};
}
