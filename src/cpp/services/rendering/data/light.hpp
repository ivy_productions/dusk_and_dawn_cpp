#pragma once

#include <types/types.hpp>
#include <algebra/matrix.hpp>

#include "color.hpp"

namespace gid_tech::rendering::data
{
	using algebra::Matrix;

	enum class LightType : u32
	{
		Directional,
		Point,
		Cone,
	};

	struct Light
	{
		Matrix<f32, 4, 4> world_to_local{};
		Matrix<f32, 4, 4> local_to_world{};
		Color color{};
		int32_t type{};
		f32 cutoff_inner{};
		f32 cutoff_outer{};
		int32_t _pad{};
	};
}
