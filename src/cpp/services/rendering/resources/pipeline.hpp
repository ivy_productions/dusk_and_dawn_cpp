#pragma once

#include <vector>

#include <hidden/hidden.hpp>
#include <types/types.hpp>

#include "../render_manager.hpp"

using namespace std;
using namespace gid_tech::types;

namespace gid_tech::rendering::implementation
{
	class Pipeline;
}

namespace gid_tech::rendering
{
	using PipelineImplementation = Hidden<implementation::Pipeline, 40>;

	class Pipeline : public PipelineImplementation
	{
	public:
		Pipeline(RenderManager & render_manager, u32 image_count, bool is_transparent, vector<u8> vert_data, vector<u8> frag_data);
		~Pipeline();

		Pipeline(Pipeline const &) = delete;
		Pipeline & operator=(Pipeline const &) = delete;

		u32 GetImageCount() const;
		bool IsTransparent() const;
	};
}
