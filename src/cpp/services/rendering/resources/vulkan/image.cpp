#include "image.hpp"

#include <cassert>

#include "../../vulkan/render_manager.hpp"
#include "../../size.hpp"
#include "../image.hpp"

namespace gid_tech::rendering
{
	using std::move;

	Image::Image(RenderManager & render_manager, Size size, vector<data::Pixel> pixels) :
		ImageImplementation(render_manager.device, render_manager.memory_cache, render_manager.memory_transfer, VkExtent2D{size.width, size.height}, move(pixels))
	{
	}

	Image::~Image()
	{
	}

	Size Image::GetSize() const
	{
		auto extent = implementation::Image::GetExtent();
		return Size{extent.width, extent.height};
	}
	vector<data::Pixel> const & Image::GetPixels() const
	{
		return implementation::Image::GetPixels();
	}

	void Image::SetPixels(vector<data::Pixel> pixels)
	{
		implementation::Image::SetPixels(move(pixels));
	}
}

namespace gid_tech::rendering::implementation
{
	using std::move;

	Image::Image(Device & device, MemoryCache & memory_cache, MemoryTransfer & memory_transfer, VkExtent2D extent, vector<Pixel> pixels) :
		device(device),
		memory_cache(memory_cache),
		memory_transfer(memory_transfer),
		pixels(),
		extent(extent),
		image(CreateImage(device.GetDevice(), extent)),
		image_view(),
		sampler(CreateSampler(device.GetDevice()))
	{
		memory_cache.Bind(image, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

		auto & image_view = const_cast<VkImageView &>(this->image_view);
		image_view = CreateImageView(device.GetDevice(), image);
		SetPixels(move(pixels));
	}

	Image::~Image()
	{
		memory_cache.Unbind(image);
		vkDestroyImageView(device.GetDevice(), image_view, nullptr);
		vkDestroyImage(device.GetDevice(), image, nullptr);
		vkDestroySampler(device.GetDevice(), sampler, nullptr);
	}

	VkExtent2D Image::GetExtent() const
	{
		return extent;
	}

	VkDescriptorImageInfo Image::GetDescriptorInfo() const
	{
		return VkDescriptorImageInfo{
			.sampler = sampler,
			.imageView = image_view,
			.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
		};
	}

	vector<Pixel> const & Image::GetPixels() const
	{
		return pixels;
	}

	void Image::SetPixels(vector<Pixel> pixels)
	{
		assert(pixels.size() == extent.width * extent.height);

		this->pixels = move(pixels);
		memory_transfer.Transfer(
			MemoryTransfer::SrcData{
				.ptr = this->pixels.data(),
				.size = this->pixels.size() * sizeof(Pixel),
			},
			MemoryTransfer::DstImageData{
				.image = image,
				.offset = {},
				.extent = VkExtent3D{extent.width, extent.height, 1},
				.layer_offset = 0,
				.layer_count = 1,
				.new_layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
			});
	}

	VkImage Image::CreateImage(VkDevice device, VkExtent2D extent)
	{
		VkImageCreateInfo create_info{
			.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
			.imageType = VK_IMAGE_TYPE_2D,
			.format = VK_FORMAT_R8G8B8A8_UNORM,
			.extent = {extent.width, extent.height, 1},
			.mipLevels = 1,
			.arrayLayers = 1,
			.samples = VK_SAMPLE_COUNT_1_BIT,
			.tiling = VK_IMAGE_TILING_OPTIMAL,
			.usage = VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT,
			.sharingMode = VK_SHARING_MODE_EXCLUSIVE,
			.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
		};

		VkImage image{};
		vkCreateImage(device, &create_info, nullptr, &image);
		return image;
	}

	VkImageView Image::CreateImageView(VkDevice device, VkImage image)
	{
		VkImageViewCreateInfo create_info{
			.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
			.image = image,
			.viewType = VK_IMAGE_VIEW_TYPE_2D,
			.format = VK_FORMAT_R8G8B8A8_UNORM,
			.subresourceRange = VkImageSubresourceRange{
				.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
				.baseMipLevel = 0,
				.levelCount = 1,
				.baseArrayLayer = 0,
				.layerCount = 1,
			},
		};

		VkImageView image_view{};
		vkCreateImageView(device, &create_info, nullptr, &image_view);
		return image_view;
	}

	VkSampler Image::CreateSampler(VkDevice device)
	{
		VkSamplerCreateInfo create_info{
			.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
			.magFilter = VK_FILTER_LINEAR,
			.minFilter = VK_FILTER_LINEAR,
			.mipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST,
			.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT,
			.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT,
			.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT,
			.compareOp = VK_COMPARE_OP_ALWAYS,
			.borderColor = VK_BORDER_COLOR_INT_OPAQUE_WHITE,
		};

		VkSampler image_sampler{};
		vkCreateSampler(device, &create_info, nullptr, &image_sampler);
		return image_sampler;
	}
}
