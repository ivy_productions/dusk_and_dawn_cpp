#include "material.hpp"

#include <cassert>

#include "../../vulkan/render_manager.hpp"
#include "../material.hpp"

namespace gid_tech::rendering
{
	using std::move;

	Material::Material(RenderManager & render_manager, vector<u32> data) :
		MaterialImplementation(render_manager.device, render_manager.memory_cache, render_manager.memory_transfer, move(data))
	{
	}

	Material::~Material()
	{
	}

	vector<u32> const & Material::Get() const
	{
		return MaterialImplementation::Get();
	}

	void Material::Set(vector<u32> data)
	{
		MaterialImplementation::Set(move(data));
	}
}

namespace gid_tech::rendering::implementation
{
	Material::Material(Device & device, MemoryCache & memory_cache, MemoryTransfer & memory_transfer, vector<u32> data) :
		device(device),
		memory_cache(memory_cache),
		memory_transfer(memory_transfer),
		size(data.size() * sizeof(data[0])),
		data(),
		buffer(CreateBuffer(device.GetDevice(), size))
	{
		memory_cache.Bind(buffer, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
		Set(move(data));
	}

	Material::~Material()
	{
		memory_cache.Unbind(buffer);
		vkDestroyBuffer(device.GetDevice(), buffer, nullptr);
	}

	VkDescriptorBufferInfo Material::GetDescriptorInfo() const
	{
		return VkDescriptorBufferInfo{
			.buffer = buffer,
			.offset = 0,
			.range = VK_WHOLE_SIZE,
		};
	}

	vector<u32> const & Material::Get() const
	{
		return data;
	}

	void Material::Set(vector<u32> data)
	{
		assert(data.size() * sizeof(data[0]) == size);
		this->data = move(data);

		memory_transfer.Transfer(
			MemoryTransfer::SrcData{
				.ptr = this->data.data(),
				.size = this->data.size() * sizeof(data[0]),
			},
			MemoryTransfer::DstBufferData{
				.buffer = buffer,
				.offset = 0,
			});
	}

	VkBuffer Material::CreateBuffer(VkDevice device, usize size)
	{
		VkBufferCreateInfo create_info{
			.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
			.size = size,
			.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
			.sharingMode = VK_SHARING_MODE_EXCLUSIVE,
		};
		VkBuffer buffer{};
		vkCreateBuffer(device, &create_info, nullptr, &buffer);
		return buffer;
	}
}
