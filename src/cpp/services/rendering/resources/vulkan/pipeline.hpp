#pragma once

#include <vector>

#include <vulkan/vulkan.h>

#include <types/types.hpp>

#include "../../vulkan/device.hpp"
#include "../../vulkan/draw_configs/frame_config.hpp"

using namespace std;

namespace gid_tech::rendering::implementation
{
	class Pipeline
	{
	private:
		Device & device;

		u32 const image_count;
		b8 const is_transparent;

		VkDescriptorSetLayout const instance_descriptor_set_layout;
		VkPipelineLayout const pipeline_layout;
		VkPipeline const pipeline;

	public:
		Pipeline(Device & device, FrameConfig const & frame_config, u32 image_count, bool is_transparent, vector<u8> vert_data, vector<u8> frag_data);
		~Pipeline();

		Pipeline(Pipeline const &);
		Pipeline & operator=(Pipeline const &);


		u32 GetImageCount() const;
		b8 IsTransparent() const;

		VkDescriptorSetLayout GetInstanceDescriptorSetLayout() const;
		VkPipelineLayout GetPipelineLayout() const;
		VkPipeline GetPipeline() const;

	private:
		static VkDescriptorSetLayout CreateInstanceDescriptorSetLayout(VkDevice device, u32 image_count);
		static VkPipelineLayout CreatePipelineLayout(VkDevice device, VkDescriptorSetLayout common_descriptor_set_layout, VkDescriptorSetLayout instance_descriptor_set_layout);
		static VkPipeline CreatePipeline(
			VkDevice device,
			VkRenderPass render_pass,
			VkPipelineLayout pipeline_layout,
			bool is_transparent,
			vector<u8> const & vert_data,
			vector<u8> const & frag_data);
	};
}
