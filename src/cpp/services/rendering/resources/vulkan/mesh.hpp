#pragma once

#include <vulkan/vulkan.h>

#include <algebra/vector.hpp>

#include "../../vulkan/device.hpp"
#include "../../vulkan/memory/memory_cache.hpp"
#include "../../vulkan/memory/memory_transfer.hpp"

#include "../../data/mesh.hpp"

namespace gid_tech::rendering::implementation
{
	using namespace std;
	using namespace algebra;

	class Mesh
	{
	public:
		struct Bounds
		{
			Vector<f32, 3> min;
			Vector<f32, 3> max;
		};

	private:
		Device & device;
		MemoryCache & memory_cache;
		MemoryTransfer & memory_transfer;

		Bounds bounds;
		usize const vertex_count;
		usize const index_count;
		vector<data::Vertex> vertices;
		vector<data::Index> indices;

		VkBuffer vertex_buffer;
		VkBuffer index_buffer;

	public:
		Mesh(Device & device, MemoryCache & memory_cache, MemoryTransfer & memory_transfer, vector<data::Vertex> vertices, vector<data::Index> indices);
		~Mesh();

		Mesh(Mesh const &) = delete;
		Mesh & operator=(Mesh const &) = delete;


		VkBuffer GetVertexBuffer() const;
		VkBuffer GetIndexBuffer() const;
		u32 GetIndexCount() const;

		vector<data::Vertex> const & GetVertices() const;
		vector<data::Index> const & GetIndices() const;
		void Set(vector<data::Vertex> vertices, vector<data::Index> indices);

		Bounds const & GetBounds();

	private:
		static VkBuffer CreateVertexBuffer(VkDevice device, usize size);
		static VkBuffer CreateIndexBuffer(VkDevice device, usize size);
	};
}
