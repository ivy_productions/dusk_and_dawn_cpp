#pragma once

#include <vector>

#include <vulkan/vulkan.h>

#include <types/types.hpp>

#include "../../vulkan/device.hpp"
#include "../../vulkan/memory/memory_cache.hpp"
#include "../../vulkan/memory/memory_transfer.hpp"

#include "../../data/pixel.hpp"

namespace gid_tech::rendering::implementation
{
	using data::Pixel;
	using std::vector;

	class Image
	{
	private:
		Device & device;
		MemoryCache & memory_cache;
		MemoryTransfer & memory_transfer;

		vector<Pixel> pixels;
		VkExtent2D const extent;

		VkImage const image;
		VkImageView const image_view;
		VkSampler const sampler;

	public:
		Image(Device & device, MemoryCache & memory_cache, MemoryTransfer & memory_transfer, VkExtent2D extent, vector<Pixel> pixels);
		~Image();

		Image(Image const &) = delete;
		Image & operator=(Image const &) = delete;


		VkExtent2D GetExtent() const;
		VkDescriptorImageInfo GetDescriptorInfo() const;

		vector<Pixel> const & GetPixels() const;
		void SetPixels(vector<Pixel> pixels);

	private:
		VkImage CreateImage(VkDevice device, VkExtent2D extent);
		VkImageView CreateImageView(VkDevice device, VkImage image);
		VkSampler CreateSampler(VkDevice device);
	};
}
