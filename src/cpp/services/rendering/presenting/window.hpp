#pragma once

#include <hidden/hidden.hpp>

#include "../render_manager.hpp"
#include "../size.hpp"
#include "../targets/frame.hpp"

namespace gid_tech::rendering::implementation
{
	class Window;
}

namespace gid_tech::rendering
{
	using WindowImplementation = Hidden<implementation::Window, 72>;

	class Window : public WindowImplementation
	{
	public:
		struct NativeWindowData
		{
			void * window_manager{};
			void * window{};
		};

	public:
		Window(RenderManager & render_manager, NativeWindowData native_window_data);
		~Window();

		Window(Window const &) = delete;
		Window & operator=(Window const &) = delete;


		Size GetSize();

		void Present(Frame const & frame);
	};
}
