#include "window.hpp"

#include <algorithm>
#include <tuple>

#include <heap/new.hpp>

#include "../../targets/vulkan/frame.hpp"
#include "../../vulkan/render_manager.hpp"
#include "../window.hpp"

namespace gid_tech::rendering
{
	Window::Window(RenderManager & render_manager, NativeWindowData native_window_data) :
		WindowImplementation(render_manager.device, implementation::Window::NativeWindowData{native_window_data.window_manager, native_window_data.window})
	{
	}

	Window::~Window()
	{
	}

	void Window::Present(Frame const & frame)
	{
		WindowImplementation::Present(frame);
	}

	Size Window::GetSize()
	{
		auto extent = WindowImplementation::GetExtent();
		return {extent.width, extent.height};
	}
}

namespace gid_tech::rendering::implementation
{
	using namespace std;

	Window::Window(Device & device, NativeWindowData native_window_data) :
		device(device),
		surface(CreateSurface(device.GetInstance(), native_window_data)),
		command_pool(CreateCommandPool(device.GetDevice(), device.GetQueueIndex(VK_QUEUE_GRAPHICS_BIT))),
		image_aquired_semaphore(CreateSemaphore(device.GetDevice())),
		image_presented_semaphore(CreateSemaphore(device.GetDevice())),
		aquire_fence(CreateFence(device.GetDevice())),
		present_fence(CreateFence(device.GetDevice()))
	{
		auto graphics_queue_index = device.GetQueueIndex(VK_QUEUE_GRAPHICS_BIT);

		VkBool32 is_supported = VK_FALSE;
		auto result = vkGetPhysicalDeviceSurfaceSupportKHR(device.GetPhysicalDevice(), graphics_queue_index, surface, &is_supported);
		if (result != VK_SUCCESS || !is_supported)
			exit(EXIT_FAILURE);

		auto [swapchain, extent] = CreateSwapchain(device.GetPhysicalDevice(), device.GetDevice(), surface, nullptr);
		this->swapchain = swapchain;
		this->swapchain_extent = extent;
	}

	Window::~Window()
	{
		vkDestroyFence(device.GetDevice(), aquire_fence, nullptr);
		vkDestroyFence(device.GetDevice(), present_fence, nullptr);
		vkDestroySemaphore(device.GetDevice(), image_aquired_semaphore, nullptr);
		vkDestroySemaphore(device.GetDevice(), image_presented_semaphore, nullptr);
		vkDestroyCommandPool(device.GetDevice(), command_pool, nullptr);
		vkDestroySwapchainKHR(device.GetDevice(), swapchain, nullptr);
		vkDestroySurfaceKHR(device.GetInstance(), surface, nullptr);
	}

	VkExtent2D Window::GetExtent()
	{
		return swapchain_extent;
	}

	void Window::Present(Frame const & frame)
	{
		Present(frame.GetColorImage(), frame.GetExtent());
	}

	void Window::Present(VkImage src_image, VkExtent2D src_extent)
	{
		u32 image_index{};
		for (;;)
		{
			VkResult result = vkAcquireNextImageKHR(device.GetDevice(), swapchain, UINT64_MAX, image_aquired_semaphore, aquire_fence, &image_index);
			if (result == VK_SUCCESS)
				break;

			VkSwapchainKHR old_swapchain = swapchain;
			auto [swapchain, extent] = CreateSwapchain(
				device.GetPhysicalDevice(),
				device.GetDevice(),
				surface,
				old_swapchain);
			this->swapchain = swapchain,
			this->swapchain_extent = extent;
			vkDestroySwapchainKHR(device.GetDevice(), old_swapchain, nullptr);
			return;
		}

		vkWaitForFences(device.GetDevice(), 1, &aquire_fence, VK_TRUE, UINT64_MAX);
		vkResetFences(device.GetDevice(), 1, &aquire_fence);

		u32 image_count{};
		vkGetSwapchainImagesKHR(device.GetDevice(), swapchain, &image_count, nullptr);
		VkImage swapchain_images[image_count];
		vkGetSwapchainImagesKHR(device.GetDevice(), swapchain, &image_count, swapchain_images);
		VkImage present_image = swapchain_images[image_index];

		VkCommandBufferAllocateInfo command_buffer_allocate_info{
			.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
			.commandPool = command_pool,
			.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
			.commandBufferCount = 1,
		};
		VkCommandBuffer command_buffer{};
		vkAllocateCommandBuffers(device.GetDevice(), &command_buffer_allocate_info, &command_buffer);
		VkCommandBufferBeginInfo command_buffer_begin_info{.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO};
		vkBeginCommandBuffer(command_buffer, &command_buffer_begin_info);

		VkImageSubresourceRange src_range{
			.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
			.levelCount = 1,
			.layerCount = 1,
		};

		VkImageSubresourceRange present_range{
			.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
			.levelCount = 1,
			.layerCount = 1,
		};

		auto graphics_queue_index = device.GetQueueIndex(VK_QUEUE_GRAPHICS_BIT);

		VkImageMemoryBarrier present_barrier{
			.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
			.srcAccessMask = 0,
			.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
			.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
			.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
			.srcQueueFamilyIndex = graphics_queue_index,
			.dstQueueFamilyIndex = graphics_queue_index,
			.image = present_image,
			.subresourceRange = present_range,
		};
		VkImageMemoryBarrier src_barrier{
			.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
			.srcAccessMask = 0,
			.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT,
			.oldLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
			.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
			.srcQueueFamilyIndex = graphics_queue_index,
			.dstQueueFamilyIndex = graphics_queue_index,
			.image = src_image,
			.subresourceRange = src_range,
		};
		vkCmdPipelineBarrier(command_buffer, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, 1, &present_barrier);
		vkCmdPipelineBarrier(command_buffer, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, 1, &src_barrier);

		VkImageBlit blit = {
			.srcSubresource = VkImageSubresourceLayers{.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT, .layerCount = 1},
			.srcOffsets = {VkOffset3D{0, 0, 0}, VkOffset3D{static_cast<int32_t>(src_extent.width), static_cast<int32_t>(src_extent.height), 1}},
			.dstSubresource = VkImageSubresourceLayers{.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT, .layerCount = 1},
			.dstOffsets = {VkOffset3D{0, 0, 0}, VkOffset3D{static_cast<int32_t>(swapchain_extent.width), static_cast<int32_t>(swapchain_extent.height), 1}},
		};
		vkCmdBlitImage(command_buffer, src_image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, present_image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &blit, VK_FILTER_NEAREST);

		VkImageMemoryBarrier present_barrier_2{
			.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
			.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
			.dstAccessMask = 0,
			.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
			.newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
			.srcQueueFamilyIndex = graphics_queue_index,
			.dstQueueFamilyIndex = graphics_queue_index,
			.image = present_image,
			.subresourceRange = present_range,
		};
		VkImageMemoryBarrier src_barrier_2{
			.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
			.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT,
			.dstAccessMask = 0,
			.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
			.newLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
			.srcQueueFamilyIndex = graphics_queue_index,
			.dstQueueFamilyIndex = graphics_queue_index,
			.image = src_image,
			.subresourceRange = present_range,
		};
		vkCmdPipelineBarrier(command_buffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, 0, 0, nullptr, 0, nullptr, 1, &present_barrier_2);
		vkCmdPipelineBarrier(command_buffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, 0, 0, nullptr, 0, nullptr, 1, &src_barrier_2);

		vkEndCommandBuffer(command_buffer);

		VkPipelineStageFlags wait_dst_stage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
		VkSubmitInfo submit_info{
			.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
			.waitSemaphoreCount = 1,
			.pWaitSemaphores = &image_aquired_semaphore,
			.pWaitDstStageMask = &wait_dst_stage,
			.commandBufferCount = 1,
			.pCommandBuffers = &command_buffer,
			.signalSemaphoreCount = 1,
			.pSignalSemaphores = &image_presented_semaphore,
		};
		device.SubmitToQueue(graphics_queue_index, submit_info, present_fence);

		vkWaitForFences(device.GetDevice(), 1, &present_fence, VK_TRUE, UINT64_MAX);
		vkResetFences(device.GetDevice(), 1, &present_fence);

		VkPresentInfoKHR present_info{
			.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
			.waitSemaphoreCount = 1,
			.pWaitSemaphores = &image_presented_semaphore,
			.swapchainCount = 1,
			.pSwapchains = &swapchain,
			.pImageIndices = &image_index,
		};
		device.PresentWithQueue(graphics_queue_index, present_info);

		//vkWaitForFences(device.GetDevice(), 1, &present_fence, VK_TRUE, UINT64_MAX);
		vkFreeCommandBuffers(device.GetDevice(), command_pool, 1, &command_buffer);
	}

	VkSurfaceKHR Window::CreateSurface(VkInstance instance, NativeWindowData native_window_data)
	{
		VkSurfaceKHR surface{};
#ifdef LINUX
		VkXlibSurfaceCreateInfoKHR create_info{
			.sType = VK_STRUCTURE_TYPE_XLIB_SURFACE_CREATE_INFO_KHR,
			.dpy = static_cast<::Display *>(native_window_data.window_manager),
			.window = static_cast<::Window>((usize) native_window_data.window),
		};
		vkCreateXlibSurfaceKHR(instance, &create_info, nullptr, &surface);
#elif WINDOWS
		VkWin32SurfaceCreateInfoKHR create_info{
			.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR,
			.hinstance = static_cast<HINSTANCE>(native_window_data.window_manager),
			.hwnd = static_cast<HWND>(native_window_data.window),
		};
		vkCreateWin32SurfaceKHR(instance, &create_info, nullptr, &surface);
#endif
		return surface;
	}

	tuple<VkSwapchainKHR, VkExtent2D> Window::CreateSwapchain(VkPhysicalDevice physical_device, VkDevice device, VkSurfaceKHR surface, VkSwapchainKHR old_swapchain)
	{
		VkSurfaceCapabilitiesKHR physical_device_surface_capabilities{};
		vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physical_device, surface, &physical_device_surface_capabilities);

		u32 surface_formats_count{};
		vkGetPhysicalDeviceSurfaceFormatsKHR(physical_device, surface, &surface_formats_count, nullptr);
		VkSurfaceFormatKHR surface_formats[surface_formats_count];
		vkGetPhysicalDeviceSurfaceFormatsKHR(physical_device, surface, &surface_formats_count, surface_formats);

		VkSurfaceFormatKHR surface_format = surface_formats[0];
		for (usize i = 0; i < surface_formats_count; i++)
		{
			VkSurfaceFormatKHR format = surface_formats[i];
			if (format.format == VK_FORMAT_B8G8R8A8_UNORM && format.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
			{
				surface_format = format;
				break;
			}
		}

		VkSwapchainCreateInfoKHR create_info{
			.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
			.surface = surface,
			.minImageCount = physical_device_surface_capabilities.minImageCount + 1,
			.imageFormat = surface_format.format,
			.imageColorSpace = surface_format.colorSpace,
			.imageExtent = physical_device_surface_capabilities.currentExtent,
			.imageArrayLayers = 1,
			.imageUsage = VK_IMAGE_USAGE_TRANSFER_DST_BIT,
			.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE,
			.preTransform = physical_device_surface_capabilities.currentTransform,
			.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
			.presentMode = VK_PRESENT_MODE_IMMEDIATE_KHR,
			.clipped = VK_TRUE,
			.oldSwapchain = old_swapchain,
		};

		VkSwapchainKHR swapchain;
		vkCreateSwapchainKHR(device, &create_info, nullptr, &swapchain);

		return make_tuple(swapchain, physical_device_surface_capabilities.currentExtent);
	}

	VkCommandPool Window::CreateCommandPool(VkDevice device, u32 queue_index)
	{
		VkCommandPoolCreateInfo create_info{
			.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
			.queueFamilyIndex = queue_index,
		};

		VkCommandPool command_pool{};
		vkCreateCommandPool(device, &create_info, nullptr, &command_pool);
		return command_pool;
	}

	VkSemaphore Window::CreateSemaphore(VkDevice device)
	{
		VkSemaphoreCreateInfo create_info{
			.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
		};

		VkSemaphore semaphore{};
		vkCreateSemaphore(device, &create_info, nullptr, &semaphore);
		return semaphore;
	}

	VkFence Window::CreateFence(VkDevice device)
	{
		VkFenceCreateInfo create_info{
			.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
		};
		VkFence fence;
		vkCreateFence(device, &create_info, nullptr, &fence);
		return fence;
	}
}
