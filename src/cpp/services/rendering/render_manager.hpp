#pragma once

#include <string>

#include <hidden/hidden.hpp>

using namespace std;

namespace gid_tech::rendering::implementation
{
	class RenderManager;
}

namespace gid_tech::rendering
{
	using RenderManagerImplementation = Hidden<implementation::RenderManager, 752>;

	class RenderManager : public RenderManagerImplementation
	{
	public:
		RenderManager(string const & identifier);
		~RenderManager();

		RenderManager(RenderManager const &) = delete;
		RenderManager & operator=(RenderManager const &) = delete;
	};
}
