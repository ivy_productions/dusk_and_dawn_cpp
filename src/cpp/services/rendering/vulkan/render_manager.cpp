#include "render_manager.hpp"

namespace gid_tech::rendering::implementation
{
	RenderManager::RenderManager(string const & identifier) :
		device(identifier),
		memory_cache(device, 2048 * 2048 * 4 * 4),
		memory_transfer(device, 2048 * 2048 * 4),
		frame_config(device)
	{
	}
}

#include "../render_manager.hpp"

namespace gid_tech::rendering
{
	RenderManager::RenderManager(string const & identifier) :
		RenderManagerImplementation(identifier)
	{
	}

	RenderManager::~RenderManager()
	{
	}
}
