#pragma once

#include <vulkan/vulkan.h>

#include "../device.hpp"

namespace gid_tech::rendering::implementation
{
	class FrameConfig
	{
	private:
		Device & device;

		VkRenderPass render_pass;
		VkDescriptorSetLayout const common_descriptor_set_layout;

	public:
		FrameConfig(Device & device);
		~FrameConfig();

		FrameConfig(FrameConfig const &) = delete;
		FrameConfig & operator=(FrameConfig const &) = delete;


		VkRenderPass GetRenderPass() const;
		VkDescriptorSetLayout GetCommonDescriptorSetLayout() const;

	private:
		static VkRenderPass CreateRenderPass(VkDevice device);
		static VkDescriptorSetLayout CreateCommonDescriptorSetLayout(VkDevice device);
	};
}
