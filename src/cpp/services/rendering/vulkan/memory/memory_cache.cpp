#include "memory_cache.hpp"

#include <cassert>

namespace gid_tech::rendering::implementation
{
	using std::make_unique;

	MemoryCache::MemoryCache(Device & device, usize block_size) :
		device(device),
		block_size(block_size)
	{
	}

	void MemoryCache::Bind(VkBuffer buffer, VkMemoryPropertyFlags flags)
	{
		VkMemoryRequirements memory_requirements {};
		vkGetBufferMemoryRequirements(device.GetDevice(), buffer, &memory_requirements);
		assert(block_size > memory_requirements.size);

		auto index = device.GetMemoryIndex(memory_requirements.memoryTypeBits, flags);

		for (auto & block : blocks)
		{
			if (block->GetMemoryTypeIndex() == index)
			{
				if (block->TryBind(buffer))
					return;
			}
		}

		auto & new_block = blocks.emplace_back(make_unique<MemoryBlock>(device, index, block_size));
		auto inserted = new_block->TryBind(buffer);
		assert(inserted);
	}

	void MemoryCache::Unbind(VkBuffer buffer)
	{
		usize index = 0;
		for (auto & block : blocks)
		{
			if (block->TryUnbind(buffer))
			{
				if (block->IsEmpty())
					blocks.erase(blocks.begin() + index);
				return;
			}
			index += 1;
		}
		assert(false);
	}

	void MemoryCache::Bind(VkImage image, VkMemoryPropertyFlags flags)
	{
		VkMemoryRequirements memory_requirements {};
		vkGetImageMemoryRequirements(device.GetDevice(), image, &memory_requirements);
		assert(block_size > memory_requirements.size);

		auto index = device.GetMemoryIndex(memory_requirements.memoryTypeBits, flags);

		for (auto & block : blocks)
		{
			if (block->GetMemoryTypeIndex() == index)
			{
				if (block->TryBind(image))
					return;
			}
		}

		auto & new_block = blocks.emplace_back(make_unique<MemoryBlock>(device, index, block_size));
		auto inserted = new_block->TryBind(image);
		assert(inserted);
	}

	void MemoryCache::Unbind(VkImage image)
	{
		usize index = 0;
		for (auto & block : blocks)
		{
			if (block->TryUnbind(image))
			{
				if (block->IsEmpty())
					blocks.erase(blocks.begin() + index);
				return;
			}
			index += 1;
		}
		assert(false);
	}
}
