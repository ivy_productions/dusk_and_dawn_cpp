#pragma once

#include <aggregates/optional.hpp>
#include <tuple>
#include <variant>
#include <vector>
#include <vulkan/vulkan.h>

#include "../device.hpp"

using namespace std;
using namespace gid_tech::aggregates;

namespace gid_tech::rendering::implementation
{
	class MemoryBlock
	{
	private:
		struct BlockRegion
		{
			usize offset;
			usize size;
		};
		struct UsedBlockRegion : BlockRegion
		{
			variant<VkBuffer, VkImage> user;
		};

	private:
		Device & device;

		u32 const memory_type_index;
		usize const size;
		VkDeviceMemory const memory;

		vector<UsedBlockRegion> used_block_regions;

	public:
		MemoryBlock(Device & device, u32 memory_type_index, usize size);
		~MemoryBlock();

		MemoryBlock(MemoryBlock const &) = delete;
		MemoryBlock & operator=(MemoryBlock const &) = delete;


		u32 GetMemoryTypeIndex() const;
		bool IsEmpty() const;

		bool TryBind(VkBuffer buffer);
		bool TryUnbind(VkBuffer buffer);
		bool TryBind(VkImage image);
		bool TryUnbind(VkImage image);

	private:
		Optional<tuple<BlockRegion, usize>> TryFindFreeBlock(VkMemoryRequirements const & memory_requirements) const;

		static VkDeviceMemory CreateDeviceMemory(VkDevice device, u32 memory_type_index, usize size);
	};
}
