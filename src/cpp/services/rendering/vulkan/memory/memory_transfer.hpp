#pragma once

#include <vulkan/vulkan.h>

#include "../device.hpp"

namespace gid_tech::rendering::implementation
{
	class MemoryTransfer
	{
	public:
		struct SrcData
		{
			void * ptr;
			usize size;
		};
		struct DstBufferData
		{
			VkBuffer buffer;
			usize offset;
		};
		struct DstImageData
		{
			VkImage image;
			VkOffset3D offset;
			VkExtent3D extent;
			u32 layer_offset;
			u32 layer_count;
			VkImageLayout new_layout;
		};

	private:
		Device & device;

		usize const stage_size;

		VkBuffer const buffer;
		VkMemoryRequirements const memory_requirements;
		VkDeviceMemory const memory;
		VkCommandPool const command_pool;
		VkFence const fence;

	public:
		MemoryTransfer(Device & device, usize stage_size);
		~MemoryTransfer();

		MemoryTransfer(MemoryTransfer const &) = delete;
		MemoryTransfer & operator=(MemoryTransfer const &) = delete;

		void Transfer(SrcData src_data, DstBufferData dst_data);
		void Transfer(SrcData src_data, DstImageData dst_data);

	private:
		static VkBuffer CreateBuffer(VkDevice device, VkDeviceSize size);
		static VkMemoryRequirements GetMemoryRequirements(VkDevice device, VkBuffer buffer);
		static VkDeviceMemory CreateMemory(VkDevice device, VkBuffer buffer, u32 memory_type_index, VkDeviceSize size);
		static VkCommandPool CreateCommandPool(VkDevice device, u32 queue_index);
		static VkFence CreateFence(VkDevice device);
	};
}
