#include "device.hpp"

#include <algorithm>
#include <collections/array.hpp>

using namespace std;
using namespace gid_tech::collections;

namespace gid_tech::rendering::implementation
{
	Device::Device(string const & application_name) :
		instance(CreateInstance(application_name)),
		physical_device(FindPhysicalDevice(instance)),
		physical_device_memory_properties(FindPhysicalDeviceMemoryProperties(physical_device)),
		queue_family_properties(FindQueueFamilyProperties(physical_device)),
		device(CreateDevice(physical_device, queue_family_properties)),
		queues(FindQueues(queue_family_properties, device)),
		queue_mutexes(queues.size())
	{
	}

	Device::~Device()
	{
		vkDestroyDevice(device, nullptr);
		vkDestroyInstance(instance, nullptr);
	}

	VkInstance Device::GetInstance() const
	{
		return instance;
	}

	VkPhysicalDevice Device::GetPhysicalDevice() const
	{
		return physical_device;
	}

	VkDevice Device::GetDevice() const
	{
		return device;
	}

	u32 Device::GetMemoryIndex(u32 memory_bits, VkMemoryPropertyFlags memory_flags) const
	{
		auto count = physical_device_memory_properties.memoryTypeCount;
		for (u32 i = 0; i < count; i++)
		{
			auto f = physical_device_memory_properties.memoryTypes[i].propertyFlags;
			if ((memory_bits & (1 << i)) > 0 && (memory_flags & f) > 0)
				return i;
		}
		return 0;
	}

	u32 Device::GetQueueIndex(VkQueueFlags flags) const
	{
		auto graphics_queue_it = find_if(queue_family_properties.begin(), queue_family_properties.end(), [flags](VkQueueFamilyProperties const & p) {
			return (p.queueFlags & flags) > 0;
		});
		return graphics_queue_it - queue_family_properties.begin();
	}

	void Device::SubmitToQueue(u32 queue_index, VkSubmitInfo const & submit_info, VkFence fence)
	{
		lock_guard queue_guard(queue_mutexes[queue_index]);
		vkQueueSubmit(queues[queue_index], 1, &submit_info, fence);
	}

	void Device::PresentWithQueue(u32 queue_index, VkPresentInfoKHR const & present_info)
	{
		lock_guard queue_guard(queue_mutexes[queue_index]);
		vkQueuePresentKHR(queues[queue_index], &present_info);
	}

	void Device::WaitDeviceIdle()
	{
		vkDeviceWaitIdle(device);
	}

	VkInstance Device::CreateInstance(string const & application_name)
	{
		VkApplicationInfo application_info = {
			.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
			.pApplicationName = application_name.c_str(),
			.applicationVersion = VK_MAKE_VERSION(0, 1, 0),
			.pEngineName = "gid_tech::rendering",
			.engineVersion = VK_MAKE_VERSION(0, 1, 0),
			.apiVersion = VK_API_VERSION_1_2,
		};

#ifdef LINUX
		Array<char const *, 2> extensions{
			VK_KHR_SURFACE_EXTENSION_NAME,
			VK_KHR_XLIB_SURFACE_EXTENSION_NAME,
		};
#elif WINDOWS
		Array<char const *, 2> extensions{
			VK_KHR_SURFACE_EXTENSION_NAME,
			VK_KHR_WIN32_SURFACE_EXTENSION_NAME,
		};
#endif


#ifdef NDEBUG
		Array<char const *, 0> validation_layers{
			// "VK_LAYER_KHRONOS_validation",
		};
#else
		Array<char const *, 1> validation_layers{
			"VK_LAYER_KHRONOS_validation",
		};
#endif

		VkInstanceCreateInfo create_info{
			.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
			.pApplicationInfo = &application_info,
			.enabledLayerCount = static_cast<u32>(validation_layers.Count()),
			.ppEnabledLayerNames = validation_layers.elements,
			.enabledExtensionCount = static_cast<u32>(extensions.Count()),
			.ppEnabledExtensionNames = extensions.elements,
		};

		VkInstance instance{};
		vkCreateInstance(&create_info, nullptr, &instance);
		return instance;
	}

	VkPhysicalDevice Device::FindPhysicalDevice(VkInstance instance)
	{
		u32 device_count = 0;
		vkEnumeratePhysicalDevices(instance, &device_count, nullptr);
		VkPhysicalDevice physical_devices[device_count];
		vkEnumeratePhysicalDevices(instance, &device_count, physical_devices);

		for (u32 i = 0; i < device_count; i++)
		{
			VkPhysicalDeviceProperties device_properties{};
			vkGetPhysicalDeviceProperties(physical_devices[i], &device_properties);
			if (device_properties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
				return physical_devices[i];
		}
		return physical_devices[0];
	}

	VkPhysicalDeviceMemoryProperties Device::FindPhysicalDeviceMemoryProperties(VkPhysicalDevice physical_device)
	{
		VkPhysicalDeviceMemoryProperties memory_properties;
		vkGetPhysicalDeviceMemoryProperties(physical_device, &memory_properties);
		return memory_properties;
	}

	vector<VkQueueFamilyProperties> Device::FindQueueFamilyProperties(VkPhysicalDevice physical_device)
	{
		u32 count = 0;
		vkGetPhysicalDeviceQueueFamilyProperties(physical_device, &count, nullptr);
		vector<VkQueueFamilyProperties> properties(count);
		vkGetPhysicalDeviceQueueFamilyProperties(physical_device, &count, properties.data());
		return properties;
	}

	VkDevice Device::CreateDevice(VkPhysicalDevice physical_device, vector<VkQueueFamilyProperties> const & queue_family_properties)
	{
		VkDeviceQueueCreateInfo device_queue_create_infos[queue_family_properties.size()];

		f32 priority = 1;
		for (usize i = 0; i < queue_family_properties.size(); i++)
		{
			device_queue_create_infos[i] = VkDeviceQueueCreateInfo{
				.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
				.queueFamilyIndex = static_cast<u32>(i),
				.queueCount = 1,
				.pQueuePriorities = &priority,
			};
		}

		VkPhysicalDeviceFeatures device_features{};

		Array<char const *, 1> device_extensions{
			VK_KHR_SWAPCHAIN_EXTENSION_NAME,
		};

		VkDeviceCreateInfo device_create_info{
			.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
			.queueCreateInfoCount = static_cast<u32>(queue_family_properties.size()),
			.pQueueCreateInfos = device_queue_create_infos,
			.enabledExtensionCount = static_cast<u32>(device_extensions.Count()),
			.ppEnabledExtensionNames = device_extensions.elements,
			.pEnabledFeatures = &device_features,
		};

		VkDevice device{};
		vkCreateDevice(physical_device, &device_create_info, nullptr, &device);
		return device;
	}

	vector<VkQueue> Device::FindQueues(vector<VkQueueFamilyProperties> const & queue_family_properties, VkDevice device)
	{
		vector<VkQueue> queues;
		for (usize i = 0; i < queue_family_properties.size(); i++)
		{
			VkQueue queue{};
			vkGetDeviceQueue(device, i, 0, &queue);
			queues.push_back(queue);
		}
		return queues;
	}
}
