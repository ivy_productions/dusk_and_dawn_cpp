#pragma once

#include <mutex>
#include <string>
#include <types/types.hpp>
#include <vector>

#include <vulkan/vulkan.h>

using namespace std;
using namespace gid_tech::types;

namespace gid_tech::rendering::implementation
{
	class Device
	{
	private:
		VkInstance const instance;
		VkPhysicalDevice const physical_device;
		VkPhysicalDeviceMemoryProperties const physical_device_memory_properties;
		vector<VkQueueFamilyProperties> const queue_family_properties;
		VkDevice const device;

		vector<VkQueue> const queues;
		vector<mutex> queue_mutexes;

	public:
		Device(string const & application_name);
		~Device();

		Device(Device const &) = delete;
		Device & operator=(Device const &) = delete;


		VkInstance GetInstance() const;
		VkPhysicalDevice GetPhysicalDevice() const;
		VkDevice GetDevice() const;

		u32 GetMemoryIndex(u32 memory_bits, VkMemoryPropertyFlags memory_flags) const;

		u32 GetQueueIndex(VkQueueFlags flags) const;
		void SubmitToQueue(u32 queue_index, VkSubmitInfo const & submit_info, VkFence fence);
		void PresentWithQueue(u32 queue_index, VkPresentInfoKHR const & present_info);
		void WaitDeviceIdle();

	private:
		static VkInstance CreateInstance(string const & name);
		static VkPhysicalDevice FindPhysicalDevice(VkInstance instance);
		static VkPhysicalDeviceMemoryProperties FindPhysicalDeviceMemoryProperties(VkPhysicalDevice physical_device);
		static vector<VkQueueFamilyProperties> FindQueueFamilyProperties(VkPhysicalDevice physical_device);
		static VkDevice CreateDevice(VkPhysicalDevice physical_device, vector<VkQueueFamilyProperties> const & queue_family_properties);
		static vector<VkQueue> FindQueues(vector<VkQueueFamilyProperties> const & queue_family_properties, VkDevice device);
	};
}
