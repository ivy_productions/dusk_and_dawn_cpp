#pragma once

#include "../data/camera.hpp"
#include "scene.hpp"

using namespace std;
using namespace gid_tech::types;

namespace gid_tech::rendering::implementation
{
	class Camera;
}

namespace gid_tech::rendering
{
	using CameraImplementation = Hidden<implementation::Camera, 16>;

	class Camera : public CameraImplementation
	{
	public:
		Camera(SceneManager & scene, data::Camera const & data);
		~Camera();

		Camera(Camera const &) = delete;
		Camera & operator=(Camera const &) = delete;


		data::Camera const & Get() const;
		void Set(data::Camera const & data);
	};
}
