#pragma once

#include "../data/light.hpp"
#include "scene.hpp"

using namespace std;

namespace gid_tech::rendering::implementation
{
	class Light;
}

namespace gid_tech::rendering
{
	using LightImplementation = Hidden<implementation::Light, 16>;

	class Light : public LightImplementation
	{
	public:
		Light(SceneManager & scene, data::Light const & data);
		~Light();

		Light(Light const &) = delete;
		Light & operator=(Light const &) = delete;


		data::Light const & Get() const;
		void Set(data::Light const & data);
	};
}
