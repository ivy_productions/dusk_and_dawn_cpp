#include "scene.hpp"

#include "../scene.hpp"

namespace gid_tech::rendering
{
	SceneManager::SceneManager(RenderManager & render_manager) :
		SceneManagerImplementation(render_manager)
	{
	}

	SceneManager::~SceneManager()
	{
	}
}

namespace gid_tech::rendering::implementation
{
	SceneManager::SceneManager(RenderManager & render_manager) :
		lights(render_manager.device, render_manager.memory_cache, render_manager.memory_transfer),
		surfaces(render_manager.device, render_manager.memory_cache, render_manager.memory_transfer),
		cameras(render_manager.device, render_manager.memory_cache, render_manager.memory_transfer)
	{
	}
}
