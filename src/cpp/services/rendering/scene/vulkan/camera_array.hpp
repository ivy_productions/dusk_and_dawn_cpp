#pragma once

#include <vulkan/vulkan.h>

#include "../../data/camera.hpp"
#include "object_array.hpp"

namespace gid_tech::rendering::implementation
{
	class CameraArray : public ObjectArray<data::Camera>
	{
	public:
		CameraArray(Device & device, MemoryCache & memory_cache, MemoryTransfer & memory_transfer) :
			ObjectArray<data::Camera>(device, memory_cache, memory_transfer, 1024)
		{
		}
	};
}
