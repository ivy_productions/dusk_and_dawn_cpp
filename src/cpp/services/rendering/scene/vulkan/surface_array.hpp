#pragma once

#include <vulkan/vulkan.h>

#include "../../data/surface.hpp"
#include "object_array.hpp"

namespace gid_tech::rendering::implementation
{
	class SurfaceArray : public ObjectArray<data::Surface>
	{
	public:
		SurfaceArray(Device & device, MemoryCache & memory_cache, MemoryTransfer & memory_transfer) :
			ObjectArray<data::Surface>(device, memory_cache, memory_transfer, 1024)
		{
		}
	};
}
