#include "camera.hpp"
#include "scene.hpp"

#include "../camera.hpp"
#include "../scene.hpp"

namespace gid_tech::rendering
{
	Camera::Camera(SceneManager & scene, data::Camera const & data) :
		CameraImplementation(scene.cameras, data)
	{
	}

	Camera::~Camera()
	{
	}

	data::Camera const & Camera::Get() const
	{
		return CameraImplementation::Get();
	}

	void Camera::Set(data::Camera const & data)
	{
		CameraImplementation::Set(data);
	}
}

namespace gid_tech::rendering::implementation
{
	Camera::Camera(CameraArray & cameras, data::Camera const & data) :
		cameras(cameras),
		index(cameras.Add(data))
	{
		Set(data);
	}

	Camera::~Camera()
	{
		cameras.Remove(index);
	}

	data::Camera const & Camera::Get() const
	{
		return cameras.Get(index);
	}

	void Camera::Set(data::Camera const & data)
	{
		cameras.Set(index, data);
	}
}
