#pragma once

#include "light_array.hpp"

namespace gid_tech::rendering::implementation
{
	class Light
	{
	public:
		LightArray & lights;
		LightArray::Index index;

	public:
		Light(LightArray & lights, data::Light const & data);
		~Light();


		data::Light const & Get() const;
		void Set(data::Light const & data);
	};
}
