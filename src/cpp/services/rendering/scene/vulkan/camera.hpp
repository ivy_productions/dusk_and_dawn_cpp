#pragma once

#include "camera_array.hpp"

namespace gid_tech::rendering::implementation
{
	class Camera
	{
	public:
		CameraArray & cameras;
		CameraArray::Index index;

	public:
		Camera(CameraArray & cameras, data::Camera const & data);
		~Camera();


		data::Camera const & Get() const;
		void Set(data::Camera const & data);
	};
}
