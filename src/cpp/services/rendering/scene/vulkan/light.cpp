#include "light.hpp"
#include "scene.hpp"

#include "../light.hpp"
#include "../scene.hpp"

namespace gid_tech::rendering
{
	Light::Light(SceneManager & scene, data::Light const & data) :
		LightImplementation(scene.lights, data)
	{
	}

	Light::~Light()
	{
	}

	data::Light const & Light::Get() const
	{
		return LightImplementation::Get();
	}

	void Light::Set(data::Light const & data)
	{
		LightImplementation::Set(data);
	}
}

namespace gid_tech::rendering::implementation
{
	Light::Light(LightArray & lights, data::Light const & data) :
		lights(lights),
		index(lights.Add(data))
	{
		Set(data);
	}

	Light::~Light()
	{
		lights.Remove(index);
	}

	data::Light const & Light::Get() const
	{
		return lights.Get(index);
	}

	void Light::Set(data::Light const & data)
	{
		lights.Set(index, data);
	}
}
