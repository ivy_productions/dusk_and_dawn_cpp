#include "frustum.hpp"

#include "bounding_box.hpp"

namespace gid_tech::bounds
{
	Frustum::Frustum(Matrix<f32, 4, 4> const & projection)
	{
		SetProjection(projection);
	}

	void Frustum::SetProjection(Matrix<f32, 4, 4> const & projection)
	{
		auto row_0 = projection.GetRow(0);
		auto row_1 = projection.GetRow(1);
		auto row_2 = projection.GetRow(2);
		auto row_3 = projection.GetRow(3);

		planes[0] = Normalize(VectorToPlane(row_3 - row_0));
		planes[1] = Normalize(VectorToPlane(row_3 + row_0));
		planes[2] = Normalize(VectorToPlane(row_3 - row_1));
		planes[3] = Normalize(VectorToPlane(row_3 + row_1));
		planes[4] = Normalize(VectorToPlane(row_3 - row_2));
		planes[5] = Normalize(VectorToPlane(row_3 + row_2));
	}

	bool Frustum::Contains(BoundingBox const & bounding_box) const
	{
		auto center = bounding_box.GetCenter();
		auto radius = bounding_box.GetRadius();

		for (auto & plane : planes)
		{
			auto distance = center.Dot(plane.normal) + plane.depth + radius;
			if (distance < 0.0)
				return false;
		}
		return true;
	}

	Frustum::Plane Frustum::VectorToPlane(Vector<f32, 4> v)
	{
		return Frustum::Plane{{v.x, v.y, v.z}, v.w};
	}

	Frustum::Plane Frustum::Normalize(Plane plane)
	{
		auto i_length = 1 / plane.normal.Length();
		return Plane{
			plane.normal * i_length,
			plane.depth * i_length,
		};
	}
}
