#pragma once

#include <algebra/vector.hpp>
#include <collections/array.hpp>

namespace gid_tech::bounds
{
	using namespace std;
	using namespace algebra;

	class BoundingBox
	{
	public:
		Vector<f32, 3> min;
		Vector<f32, 3> max;

	public:
		BoundingBox() = default;
		BoundingBox(Vector<f32, 3> min, Vector<f32, 3> max);

		Vector<f32, 3> GetSize() const;
		Vector<f32, 3> GetExtend() const;
		Vector<f32, 3> GetCenter() const;

		f32 GetRadius() const;
		f32 GetRadiusSquared() const;

		BoundingBox & Add(BoundingBox const & other);

		b8 Inside(Vector<f32, 3> point) const;

		b8 Overlaps(BoundingBox const & other) const;
		static b8 Overlaps(BoundingBox const & a, BoundingBox const & b);

		BoundingBox Transformed(Matrix<f32, 4, 4> const & trans) const;
		BoundingBox & Transform(const Matrix<f32, 4, 4> & trans);

		Array<Vector<f32, 3>, 8> GetCorners();
	};
}
