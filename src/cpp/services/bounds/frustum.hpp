#pragma once

#include <algebra/matrix.hpp>
#include <algebra/vector.hpp>
#include <collections/array.hpp>

using namespace std;
using namespace gid_tech::algebra;
using namespace gid_tech::collections;

namespace gid_tech::bounds
{
	class BoundingBox;

	class Frustum
	{
	private:
		struct Plane
		{
			Vector<f32, 3> normal{};
			f32 depth{};
		};

	private:
		Array<Plane, 6> planes{};

	public:
		Frustum(Matrix<f32, 4, 4> const & projection);

		void SetProjection(Matrix<f32, 4, 4> const & projection);
		bool Contains(BoundingBox const & bounding_box) const;

	private:
		static Plane VectorToPlane(Vector<f32, 4> v);
		static Plane Normalize(Plane plane);
	};
}
