#pragma once

#include <rendering/targets/frame.hpp>
#include <windowing/input.hpp>

#include <loading/cache.hpp>
#include <loading/scene_instance.hpp>
#include <scene/camera_node.hpp>
#include <scene/light_node.hpp>
#include <scene/node.hpp>
#include <scene/scene.hpp>

#include <10_domain/00_entities/player.hpp>
#include <10_domain/03_game/game.hpp>

using namespace std;
using namespace gid_tech;
using namespace gid_tech::aggregates;
using namespace scene;

using loading::LoadingCache;
using loading::SceneElements;
using loading::SceneInstance;

namespace lazy_rider::view::entities
{
	using namespace domain::entities;
	using namespace domain::game;

	class PlayerView
	{
	private:
		Game & game;
		Player const & player;
		LoadingCache & loading_cache;

		Node root;
		Node astronaut_root;
		Node camera_y;
		Node camera_x;
		CameraNode camera;
		Node skybox_root;
		LightNode helmet_light;

		Optional<unique_ptr<SceneInstance>> astronaut;
		Optional<unique_ptr<SceneInstance>> astronaut_won;
		SceneInstance skybox;

		vector<unique_ptr<SceneInstance>> trashes;
		vector<unique_ptr<SceneInstance>> drinks;

	public:
		PlayerView(Game & game, Player const & player, Scene & scene, shared_ptr<rendering::Frame> frame, LoadingCache & loading_cache);

		void Tick(f32 dt, windowing::Input const & input);
		void ChangeAspect(f32 aspect);

		Optional<Vector<f32, 2>> GetMouseFloorPlanePoint(windowing::Input const & input);
		static Optional<Vector<f32, 3>> IntersectFloor(Vector<f32, 3> origin, Vector<f32, 3> dir);
	};
}
