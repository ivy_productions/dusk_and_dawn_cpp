#include "material_replace.hpp"

#include <loading/material_data.hpp>

namespace lazy_rider::view::entities
{
	void ReplaceMaterials(ModelNode * node, LoadingCache & loading_cache)
	{
		for (auto & s : node->GetSurfaces())
		{
			if (s.name == "voxel238238238")
			{
				s.images = {
					loading_cache.GetOrLoadImage("images/plastic_albedo.png"),
					loading_cache.GetOrLoadImage("images/plastic_roughness.png"),
					loading_cache.GetOrLoadImage("images/plastic_normal.png"),
				};
			}
			else if (s.name == "voxel102204255")
			{
				auto data = s.material->Get();
				auto material = reinterpret_cast<MaterialData *>(data.data());
				material->color = {0.4, 0.7, 1, 1};
				s.material->Set(data);
			}
			else if (s.name == "voxel686868" || s.name == "voxel171717")
			{
				s.images = {
					loading_cache.GetOrLoadImage("images/steel_albedo.png"),
					loading_cache.GetOrLoadImage("images/steel_metalic.png"),
					loading_cache.GetOrLoadImage("images/default/normal.png"),
				};
			}
			else if (s.name == "voxel858585" || s.name == "voxel170170170")
			{
				s.images = {
					loading_cache.GetOrLoadImage("images/asteroid_albedo.png"),
					loading_cache.GetOrLoadImage("images/asteroid_roughness.png"),
					loading_cache.GetOrLoadImage("images/asteroid_normal.png"),
				};
			}
			else if (s.name == "voxel153204255" || s.name == "voxel51204255")
			{
				s.pipeline = loading_cache.GetOrLoadPipeline("shaders/lazy_rider/lit.vert.spv", "shaders/lazy_rider/lit.frag.spv", 3, true);
				auto data = s.material->Get();
				auto material = reinterpret_cast<MaterialData *>(data.data());
				material->color = {1, 1, 1, 0.5};
				material->metalicness = 0.8;
				material->roughness = 0;
				s.material->Set(data);
			}
			else if (s.name == "voxel255255204")
			{
				s.pipeline = loading_cache.GetOrLoadPipeline("shaders/lazy_rider/lit.vert.spv", "shaders/lazy_rider/lit.frag.spv", 3, true);
				auto data = s.material->Get();
				auto material = reinterpret_cast<MaterialData *>(data.data());
				material->color = {0.5, 1, 0.5, 0.6};
				material->metalicness = 0.8;
				material->roughness = 0;
				s.material->Set(data);
			}
			else if (s.name == "voxel0255102")
			{
				auto data = s.material->Get();
				auto material = reinterpret_cast<MaterialData *>(data.data());
				material->color = {0.1, 1, 0.7, 1};
				material->metalicness = 0.2;
				material->roughness = 1;
				s.material->Set(data);
			}
			else if (s.name == "voxel255153204" || s.name == "voxel25520451")
			{
				auto data = s.material->Get();
				auto material = reinterpret_cast<MaterialData *>(data.data());
				material->color = {1, 1, 0, 1};
				material->metalicness = 0.2;
				material->roughness = 1;
				s.material->Set(data);
			}
		}
	}
}
