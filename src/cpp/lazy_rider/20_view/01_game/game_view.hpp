#pragma once

#include <map>

#include <loading/cache.hpp>
#include <loading/scene_elements.hpp>
#include <loading/scene_instance.hpp>
#include <rendering/presenting/window.hpp>
#include <rendering/render_manager.hpp>
#include <rendering/targets/frame.hpp>
#include <scene/light_node.hpp>
#include <scene/scene.hpp>
#include <windowing/window.hpp>
#include <windowing/window_manager.hpp>

#include <10_domain/03_game/game.hpp>
#include <20_view/00_entities/grabbable_view.hpp>
#include <20_view/00_entities/grappling_hook_view.hpp>
#include <20_view/00_entities/player_view.hpp>

using namespace gid_tech;
using namespace scene;
using loading::LoadingCache;
using loading::MaterialData;
using loading::SceneElements;
using loading::SceneInstance;

namespace lazy_rider::view::game
{
	using namespace domain::entities;
	using namespace domain::systems;
	using namespace domain::game;
	using namespace view::entities;

	class GameView
	{
	private:
		Game & game;

		windowing::WindowManager window_manager;
		windowing::Window window;

		rendering::RenderManager render_manager;
		rendering::Window render_window;
		shared_ptr<rendering::Frame> render_frame;

		LoadingCache loading_cache;

		shared_ptr<Pipeline> opaque_pipeline;
		shared_ptr<Pipeline> transparent_pipeline;
		shared_ptr<Pipeline> unlit_pipeline;

		Scene scene;
		LightNode main_light;
		LightNode ambient_light;

		SceneInstance background_trash;
		shared_ptr<SceneElements> hook_prefab;

		// entities
		PlayerView player_view;
		Optional<unique_ptr<GrapplingHookView>> grappling_hook_view;
		vector<unique_ptr<GrabbableView>> grabbable_views;

	public:
		GameView(Game & game);

		void Tick(f32 dt);
	};
}
