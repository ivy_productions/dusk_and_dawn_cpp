// #include <delta_time/delta_timer.hpp>
// #include <filesystem>
// #include <iostream>

#include <algebra/matrix.hpp>
#include <algebra/transform.hpp>
#include <algebra/vector.hpp>
#include <collections/array.hpp>
#include <collections/list.hpp>
#include <formatting/basic.hpp>
#include <heap/allocator.hpp>
#include <math/math.hpp>
#include <parsing/basic.hpp>
#include <types/types.hpp>

#ifdef LINUX
#include <linux/syscalls.hpp>
#include <syscall.h>
using namespace gid_tech::linux;
#elif WINDOWS
#include <windows.h>
#endif

// #include "../10_domain/03_game/game.hpp"
// #include "../20_view/01_game/game_view.hpp"

// using namespace std;
using namespace gid_tech::types;
using namespace gid_tech::formatting;
using namespace gid_tech::parsing;
// using namespace gid_tech::delta_time;
using namespace gid_tech::collections;
// using namespace lazy_rider::domain::game;
// using namespace lazy_rider::view::game;

using namespace gid_tech::math;
using namespace gid_tech::algebra;

#include <initializer_list>

namespace lazy_rider::application
{
	struct Yo
	{
		i16 const f{};
		Yo(i16 f) :
			f{f}
		{
		}
		~Yo()
		{
		}
	};


	void Foo(std::initializer_list<i16> t)
	{
		List<std::initializer_list<i16>> ts;
		ts.Add(t);
	}

	void Main()
	{
		auto m = Matrix<i16, 5, 5>::Diagonal(1);
		auto m2 = new Matrix{Matrix<i16, 5, 5>::Diagonal(1)};
		delete m2;

		auto init = [&](usize i)
		{
			return Yo(m.elements[i]);
		};

		List<Yo> l1{10, ListInitFunc{init}};
		List<Yo> l2{ListInit{.allocator = Allocator::default_allocator, .block_size = 8}, 2_USIZE, 1_I16};
		l2.Add(ListIndex{1}, 8_I16);
		l2.Add(ListIndex{1}, 9999_I16);

		l2.Remove([](Yo y)
				  { return y.f == 8; });

		l2.Add({0}, 32_I16);
		l2.Add({0}, 32_I16);

		List<Yo> l3;
		for (auto & v : l2[{1, 4}])
			l3.Add(v);

		Array<c8, 14> str{"hello, world!"};
		List<c8> l4{str[{0, str.Count()}]};

		return;
	}

	extern "C" [[noreturn]] void Start()
	{
		Main();
#ifdef LINUX
		Exit(0);
#elif WINDOWS
		ExitProcess(0);
#endif
		__builtin_unreachable();
	}
}


// void main()
// {
// 	Main();

// 	filesystem::current_path("../");
// 	ios_base::sync_with_stdio(false);

// 	Game game{};
// 	GameView game_view{game};
// 	DeltaTimer delta_timer{};

// 	while (game.ShouldRun())
// 	{
// 		auto dt = delta_timer.Step();

// 		game.Tick(dt);
// 		game_view.Tick(dt);
// 	}
// }