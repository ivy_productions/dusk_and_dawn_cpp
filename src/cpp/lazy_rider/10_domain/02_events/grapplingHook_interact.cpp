#include "grapplingHook_interact.hpp"
#include "../00_entities/grabable.hpp"
#include "../00_entities/grappling_hook.hpp"

namespace lazy_rider::domain::events
{
	GrapplingHookAttach::GrapplingHookAttach(Grabable * grabable) :
		grabable(grabable)
	{
	}

	void GrapplingHookAttach::Handle(b2World & physicsWorld, Optional<GrapplingHook> & grapplingHook)
	{
		if (grapplingHook.IsNone())
			return;

		grapplingHook.Value().AttachTo(physicsWorld, grabable);
	}

	void GrapplingHookDetach::Handle(b2World & physicsWorld, Player & player, Optional<GrapplingHook> & grapplingHook)
	{
		if (grapplingHook.IsNone())
			return;

		grapplingHook.Value().Dettach(physicsWorld);

		// auto playerPos = grapplingHook.value().Position();
		// auto pos = grapplingHook.value().Position();
		// auto dir = playerPos - pos;
		// grapplingHook.value().Position(dir.Normalized());
	}

	void GrapplingHookDestroy::Handle(Optional<GrapplingHook> & grapplingHook)
	{
		grapplingHook = OptionalNone;
	}

	void GrapplingHookStopFlight::Handle(Optional<GrapplingHook> & grapplingHook)
	{
		if (grapplingHook.IsNone())
			return;

		grapplingHook.Value().flightTime = -1;
	}
}