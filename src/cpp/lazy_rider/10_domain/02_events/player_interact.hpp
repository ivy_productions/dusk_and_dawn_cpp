#pragma once

#include "../00_entities/drink.hpp"
#include "../00_entities/trash.hpp"
#include <aggregates/optional.hpp>
#include <algebra/vector.hpp>
#include <variant>

namespace lazy_rider::domain::entities
{
	struct Player;
	struct GrapplingHook;
}
namespace lazy_rider::domain::game
{
	class Game;
}

class b2World;

using namespace gid_tech::aggregates;
using gid_tech::algebra::Vector;
using namespace lazy_rider::domain::entities;
using namespace lazy_rider::domain::game;

namespace lazy_rider::domain::events
{
	struct PlayerGrapple
	{
		Vector<f32, 2> relative_position;
		PlayerGrapple(Vector<f32, 2> relative_position);

		void Handle(b2World & physics_world, Player & player, Optional<GrapplingHook> & grapplingHook);
	};

	struct PlayerThrow
	{
		Vector<f32, 2> relative_position;

		PlayerThrow(Vector<f32, 2> relative_position);
		void Handle(Player & player, Game & game);
	};

	struct PlayerPickUpTrash
	{
		Trash item;
		PlayerPickUpTrash(Trash);
		void Handle(Player & player);
	};

	struct PlayerPickUpDrink
	{
		Drink item;
		PlayerPickUpDrink(Drink);
		void Handle(Player & player);
	};
}
