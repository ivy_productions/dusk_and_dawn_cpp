#include "grapplingHook_pull.hpp"

#include "../00_entities/grappling_hook.hpp"
#include "../00_entities/player.hpp"
#include "box2d/box2d.h"

namespace lazy_rider::domain::systems
{
	void GrapplingHookPull::Tick(f32 dt, b2World & physics_world, Player & player, Optional<GrapplingHook> & grapplingHook)
	{
		if (grapplingHook.IsNone())
			return;

		auto & grapplingHookV = grapplingHook.Value();
		grapplingHookV.flightTime -= dt;

		if (grapplingHookV.IsAttached() || grapplingHookV.flightTime <= 0)
		{
			auto body1 = player.body.get();
			auto body2 = grapplingHookV.body.get();

			auto pA = body1->GetWorldPoint(b2Vec2(0, 0));
			auto pB = body2->GetWorldPoint(b2Vec2(0, 0));
			auto lenVector = pB - pA;
			auto length = lenVector.Length();

			auto targetLength = 0.f;
			auto K = 100;

			auto deltaL = length - targetLength;
			auto force = std::max(K * deltaL, 100.0f);

			//normalize the lenVector
			if (length == 0)
			{
				lenVector = b2Vec2(0.70710678118654757, 0.70710678118654757);
			}
			else
			{
				lenVector = b2Vec2(lenVector.x / length, lenVector.y / length);
			}

			auto playerMass = body1->GetMass();
			auto otherMass = body2->GetMass();

			auto playerRatio = otherMass / (otherMass + playerMass);
			auto otherRatio = 1 - playerRatio;

			auto sprForce1 = b2Vec2(lenVector.x * force * 0.5 * playerRatio, lenVector.y * force * 0.5 * playerRatio);
			auto sprForce2 = b2Vec2(lenVector.x * force * 0.5 * otherRatio, lenVector.y * force * 0.5 * otherRatio);
			body1->ApplyForce(sprForce1, pA, true);
			body2->ApplyForce(-sprForce2, pB, true);
		}
	}
}