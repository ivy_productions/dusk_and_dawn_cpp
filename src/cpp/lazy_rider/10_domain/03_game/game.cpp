#include "game.hpp"
#include "box2d/box2d.h"
#include <algorithm>

void b2DestroyHelper::SayGoodbye(b2Joint * joint)
{
	joint->GetUserData().RaiseDestroyEvent();
}
void b2DestroyHelper::SayGoodbye(b2Fixture * fixture){};

namespace lazy_rider::domain::game
{
	Game::Game() :
		physics_world(b2Vec2{0.0f, 0.0f}),
		player(physics_world),
		grapplingHook(OptionalNone),
		contactResolver(*this),
		grabableSpawner(
			10,
			50,
			0.3,
			0.1,
			10,
			2,
			{
				Trash{10, 0.5f, 1, true},
				Trash{10, 0.7f, 0.7f, false},
			},
			Drink{}),
		winCondition{
			5,
			3,
		}
	{
		physics_world.SetContactListener(&contactResolver);
		physics_world.SetDestructionListener(&destroyHelper);
	}

	Player const & Game::GetPlayer() const
	{
		return player;
	}

	Optional<GrapplingHook> const & Game::GetGrapplingHook() const
	{
		return grapplingHook;
	}

	std::vector<std::unique_ptr<Grabable>> const & Game::GetGrabbables() const
	{
		return grabables;
	}

	void Game::Tick(f32 dt)
	{
		HandleEvents(grapplingHook_detach, physics_world, player, grapplingHook);
		HandleEvents(grapplingHook_destroy, grapplingHook);
		HandleEvents(grapplingHook_stopFlight, grapplingHook);
		HandleEvents(grabable_remove, *this);

		HandleEvents(player_trashpickups, player);
		HandleEvents(player_drinkpickups, player);

		//player can no longer move when won
		if (!player.hasWon)
		{
			HandleEvents(player_throws, player, *this);
			HandleEvents(player_grapple, physics_world, player, grapplingHook);
		}

		HandleEvents(grapplingHook_attach, physics_world, grapplingHook);

		grapplingHookPull.Tick(dt, physics_world, player, grapplingHook);
		grabableSpawner.Tick(dt, *this, player);
		winCondition.Tick(player);

		physics_world.Step(dt, 6, 2);
	}

	bool Game::ShouldRun()
	{
		return should_run;
	}

	void Game::Stop()
	{
		should_run = false;
	}

	void Game::RemoveGrabable(Grabable * grabable)
	{
		auto grabableItter = std::find_if(std::begin(grabables), std::end(grabables), [&grabable](auto && element) { return element.get() == grabable; });

		//grabable not found
		if (grabableItter == std::end(grabables))
			return;

		grabables.erase(grabableItter);
	}
}
