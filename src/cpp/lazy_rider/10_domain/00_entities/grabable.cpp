#include "grabable.hpp"
#include "box2d/box2d.h"

namespace lazy_rider::domain::entities
{
	Grabable::Grabable(b2World & physics_world, Vector<f32, 2> initalPosition, Vector<f32, 2> initalVelocity, std::variant<Trash, Drink> object) :
		object(object)
	{
		b2BodyDef bodyDef;
		bodyDef.type = b2_dynamicBody;
		bodyDef.position.Set(initalPosition.x, initalPosition.y);
		bodyDef.linearVelocity.Set(initalVelocity.x, initalVelocity.y);
		body = std::unique_ptr<b2Body, std::function<void(b2Body *)>>{
			physics_world.CreateBody(&bodyDef),
			[&physics_world](b2Body * b) {
				physics_world.DestroyBody(b);
			}};
		f32 boxHeight = 1, boxWidth = 1, boxDesinity = 1;

		if (std::holds_alternative<Trash>(object))
		{
			auto & trash = std::get<Trash>(object);
			boxHeight = trash.height;
			boxWidth = trash.width;
			boxDesinity = trash.density;
		}

		b2PolygonShape dynamicBox;
		dynamicBox.SetAsBox(boxWidth, boxHeight);

		b2FixtureDef fixtureDef;
		fixtureDef.shape = &dynamicBox;
		fixtureDef.density = boxDesinity;
		fixtureDef.friction = 0.3f;

		body->CreateFixture(&fixtureDef);
		body->GetUserData().anyObject = this;
	}

	Vector<f32, 2> Grabable::Position() const
	{
		auto physicsPosition = body->GetPosition();
		return {physicsPosition.x, physicsPosition.y};
	}

	void Grabable::Position(Vector<f32, 2> position)
	{
		auto angle = body->GetAngle();
		body->SetTransform(b2Vec2{position.x, position.y}, angle);
	}

	f32 Grabable::Rotation() const
	{
		return body->GetAngle();
	}

	void Grabable::Rotation(f32 angle)
	{
		auto physicsPosition = body->GetPosition();
		body->SetTransform(physicsPosition, angle);
	}
}