#pragma once

#include "../services/algebra/vector.hpp"
#include <functional>
#include <memory>
#include <variant>

class b2Body;
class b2Joint;
class b2Fixture;
class b2World;

namespace lazy_rider::domain::entities
{
	using gid_tech::algebra::Vector;
	struct Grabable;

	struct GrapplingHook
	{
		std::unique_ptr<b2Body, std::function<void(b2Body *)>> body;

		b2Joint * joint{};
		Grabable * attachedGrababble{};

		f32 flightTime{};

		GrapplingHook(b2World & physics_world, Vector<f32, 2> initalPosition, Vector<f32, 2> initalVelocity);

		Vector<f32, 2> Position() const;
		void Position(Vector<f32, 2>);
		f32 Rotation() const;
		void Rotation(f32);

		void AttachTo(b2World & physics_world, Grabable * grababble);
		void Dettach(b2World & physics_world);
		bool IsAttached() const;
		Grabable * AttachedGrabable();
	};
}