#pragma once

#include <types/types.hpp>

using namespace gid_tech::types;

namespace lazy_rider::domain::entities
{
	struct Trash
	{
		f32 density = 10;
		f32 height = 1;
		f32 width = 1;
		b8 isCollectable = true;

		Trash() = default;
		Trash(f32 density, f32 height, f32 width, b8 isCollectable) :
			density(density), height(height), width(width), isCollectable(isCollectable)
		{
		}
	};

}