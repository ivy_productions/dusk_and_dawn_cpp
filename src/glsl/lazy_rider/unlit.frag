#version 450
#extension GL_ARB_separate_shader_objects : enable

#include "../services/lit/lit_frag.glsl"

layout(set = 1, binding = 10, std140) uniform Material
{
	vec4 color;
	float metalness;
	float roughness;
} material;

layout(set = 1, binding = 11) uniform sampler2D images[3]; 

bool IsLit()
{
	return false;
}

SurfaceData GetSurfaceData()
{
	return SurfaceData(
		texture(images[0], vert_to_frag.uv).rgba * material.color,
		texture(images[1], vert_to_frag.uv).rgb * material.metalness,
		texture(images[1], vert_to_frag.uv).a * material.roughness,
		vec3(0),
		texture(images[2], vert_to_frag.uv).rgb * 2.0 - 1.0
	);
}	
