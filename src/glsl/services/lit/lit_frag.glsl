#include "lit_frag_input.glsl"
#include "lit_frag_output.glsl"

struct SurfaceData
{
	vec4 diffuse;
	vec3 specular;
	float roughness;
	vec3 emissive;
	vec3 normal;
};

SurfaceData GetSurfaceData();
bool IsLit();

struct LightData
{
	vec3 diffuse;
	vec3 specular;
};

LightData CalcAmbientLight(
	in vec3 light_color,
	in vec3 surface_diffuse);
LightData CalcDirectionalLight(
	in vec3 light_direction, 
	in vec3 surface_position,
	in vec3 camera_position,
	in vec3 light_color,
	in vec3 surface_diffuse,
	in vec3 surface_specular,
	in float surface_roughness,
	in vec3 surface_normal);
LightData CalcPointLight(
	in vec3 light_position, 
	in vec3 surface_position,
	in vec3 camera_position,
	in vec3 light_color,
	in vec3 surface_diffuse,
	in vec3 surface_specular,
	in float surface_roughness,
	in vec3 surface_normal);
LightData CalcSpotLight(	
	in vec3 light_position,
	in vec3 light_direction,
	in vec3 surface_position,
	in vec3 camera_position,
	in vec3 light_color,
	in float light_cutoff_inner,
	in float light_cutoff_outer,
	in vec3 surface_diffuse,
	in vec3 surface_specular,
	in float surface_roughness,
	in vec3 surface_normal);

LightData CombineLightData(LightData a, LightData b)
{
	return LightData(a.diffuse + b.diffuse, a.specular + b.specular);
}

void main()
{
	SurfaceData surface_data = GetSurfaceData();
	if(surface_data.diffuse.a < 0.01)
		discard;

	vec3 surface_normal = normalize(vert_to_frag.tbn * surface_data.normal);

	vec3 surface_position = vert_to_frag.position;
	Camera camera = GetCamera();
	vec3 camera_position = vec3(camera.local_to_world * vec4(0, 0, 0, 1));

	if(IsLit())
	{
		LightData light_data = LightData(vec3(0, 0, 0), vec3(0, 0, 0));
		uint light_count = GetLightCount();
		for(uint i = 0; i < light_count; i++)
		{
			Light light = GetLight(i);

			if(light.type == light_type_ambient)
			{
				light_data = CombineLightData(light_data, CalcAmbientLight(
					light.color.rgb,
					surface_data.diffuse.rgb
				));
			}
			else if(light.type == light_type_directional)
			{
				vec3 light_direction = vec3(light.local_to_world[2]);
				light_data = CombineLightData(light_data, CalcDirectionalLight(
					light_direction,
					surface_position,
					camera_position,
					light.color.rgb,
					surface_data.diffuse.rgb,
					surface_data.specular,
					surface_data.roughness,
					surface_normal
				));
			}
			else if(light.type == light_type_point)
			{
				vec3 light_position = vec3(light.local_to_world * vec4(0, 0, 0, 1));
				light_data = CombineLightData(light_data, CalcPointLight(
					light_position,
					surface_position,
					camera_position,
					light.color.rgb,
					surface_data.diffuse.rgb,
					surface_data.specular,
					surface_data.roughness,
					surface_normal
				));
			}
			else if(light.type == light_type_spot)
			{
				vec3 light_position = vec3(light.local_to_world * vec4(0, 0, 0, 1));
				vec3 light_direction = vec3(light.local_to_world[2]);
				light_data = CombineLightData(light_data, CalcSpotLight(
					light_position,
					light_direction,
					surface_position,
					camera_position,
					light.color.rgb,
					light.cutoff_inner,
					light.cutoff_outer,
					surface_data.diffuse.rgb,
					surface_data.specular,
					surface_data.roughness,
					surface_normal
				));
			}
		}
	
		out_color = vec4(light_data.diffuse + light_data.specular / surface_data.diffuse.a, 0);
		out_color.a = surface_data.diffuse.a;
	}
	else
	{
		out_color = surface_data.diffuse;
	}
}

LightData CalcAmbientLight(
	in vec3 light_color,
	in vec3 surface_diffuse)
{
	return LightData(
		surface_diffuse * light_color,
		vec3(0, 0, 0)
	);
}

float CalculateAttenuation(in float dist)
{
	return 1.0 / (1 + dist * dist);
}

#define SHINE_EXP 32

LightData CalcDirectionalLight(
	in vec3 light_direction, 
	in vec3 surface_position,
	in vec3 camera_position,
	in vec3 light_color,
	in vec3 surface_diffuse,
	in vec3 surface_specular,
	in float surface_roughness,
	in vec3 surface_normal)
{
	vec3 light_dir = normalize(light_direction);
	vec3 view_dir = normalize(camera_position - surface_position);

	// diffuse shading
	float diffuse = max(dot(surface_normal, light_dir), 0.0);

	// specular shading
	vec3 halfway_dir = normalize(light_direction + view_dir);
	float specular = pow(max(dot(surface_normal, halfway_dir), 0.0), (1 - surface_roughness) * SHINE_EXP);

	// combine results
	return LightData(
		surface_diffuse * diffuse,
		surface_specular * specular
	);

}

LightData CalcPointLight(
	in vec3 light_position, 
	in vec3 surface_position,
	in vec3 camera_position,
	in vec3 light_color,
	in vec3 surface_diffuse,
	in vec3 surface_specular,
	in float surface_roughness,
	in vec3 surface_normal)
{
	vec3 light_dir = normalize(light_position - surface_position);
	vec3 view_dir = normalize(camera_position - surface_position);
	
	// diffuse shading
	float diffuse = max(dot(surface_normal, light_dir), 0.0);
	
	// specular shading
	vec3 halfway_dir = normalize(light_dir + view_dir);
	float specular = pow(max(dot(surface_normal, halfway_dir), 0.0), (1 - surface_roughness) * SHINE_EXP);
	
	// attenuation
	float dist = length(light_position - surface_position);
	float attenuation = CalculateAttenuation(dist);

	return LightData(
		surface_diffuse * light_color * (diffuse * attenuation),
		surface_specular * light_color * (specular * attenuation)
	);
}

LightData CalcSpotLight(	
	in vec3 light_position,
	in vec3 light_direction,
	in vec3 surface_position,
	in vec3 camera_position,
	in vec3 light_color,
	in float light_cutoff_inner,
	in float light_cutoff_outer,
	in vec3 surface_diffuse,
	in vec3 surface_specular,
	in float surface_roughness,
	in vec3 surface_normal)
{
	vec3 light_dir = normalize(light_position - surface_position);
	vec3 view_dir = normalize(camera_position - surface_position);
	
	// diffuse shading
	float diffuse = max(dot(surface_normal, light_dir), 0.0);
	
	// specular shading
	vec3 halfway_dir = normalize(light_dir + view_dir);
	float specular = pow(max(dot(surface_normal, halfway_dir), 0.0), (1 - surface_roughness) * SHINE_EXP);
	
	// attenuation	
	float dist = length(light_position - surface_position);
	float attenuation = CalculateAttenuation(dist);

	// spot
	float theta = dot(light_dir, normalize(light_direction));
	float epsilon = cos(light_cutoff_inner) - cos(light_cutoff_outer);
	float cutoff = clamp((theta - cos(light_cutoff_outer)) / epsilon, 0.0, 1.0);

	return LightData(
		surface_diffuse * light_color * (diffuse * attenuation * cutoff),
		surface_specular * light_color * (specular * attenuation * cutoff)
	);
}
